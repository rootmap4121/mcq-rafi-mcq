<?php
include('class/auth_index.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>MCQ</title>
        <link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/menu.css" rel="stylesheet" type="text/css" />
        <link href="css/style_1.css" rel="stylesheet" type="text/css" />
        <link href="css/background.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
        <script type="text/javascript">var _siteRoot = 'index.php', _root = 'index.php';</script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script>
            function showmed(str)
            {
                if (str == "")
                {
                    document.getElementById("subject").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("subject").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/test_paper.php?q=" + str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function showanswer(str)
            {
                if (str == "")
                {
                    document.getElementById("answer").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("answer").innerHTML = xmlhttp.responseText;
                    }
                }
                medium = document.getElementById('medium').value;
                ecategory = document.getElementById('ecategory').value;
                bcategory = document.getElementById('bcategory').value;
                
                if(medium==8)
                {
                   var category=ecategory;
                }
                else
                {
                    var category=bcategory;   
                }
                xmlhttp.open("GET", "ajax/mcq.php?a=" + str + "&d=" + medium+"&class_id="+category, true);
                xmlhttp.send();
            }
        </script>
    </head>

    <body>
        <div id="body">
            <?php include("include/head_menu.php"); ?>
            <!----body_main start here-------------------------------------------->
            <div id="body_main">
                <!----head_sec start here--------------------------------------------> 
                <div class="head_sec">

                    <div class="head_logo left">
                        <img src="img/logo.png" />
                    </div>
                    <div class="head_manu left">
                        <?php include("include/menu.php"); ?>
                    </div>

                </div>
                <!----head_sec End here-------------------------------------------->

                <!----welcome start here-------------------------------------------->
                <div id="welcom">
                    <div class="welcom_admin">

                        <div class="wlc shawdow_1 background_14">Welcome to Profile 
                            <div class="wlc_logo right">
                                <ul>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/contact(2).png" /></a></li>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/chat.png" /></a></li>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/message.png" /></a></li>
                                </ul>
                            </div> 
                        </div>

                        <div class="body_adin height_auto">
                            <?php include('include/profile.php'); ?>

                            <div class="mcq left">
                                <div class="mcq_head background_14 shawdow_1 colore_11">
                                    PSC Online MCQ Model Test
                                </div>

                                <div class="mcq_text_fild height_140 shawdow_2">
                                    <div class="text_div">
                                        <div class="text_name left colore_12">Medium :</div>
                                        <div class="text_fild left">
                                            <div class="select-style">
                                                <select onchange="showmed(this.value)" id="medium" name="medium">
                                                    <option value="">Medium</option>
                                                    <?php
                                                    $medium = $obj->SelectAll('ams_madiam');
                                                    foreach ($medium as $rowme):
                                                        ?>
                                                        <option value="<?php echo $rowme->id; ?>"><?php echo $rowme->medium_name; ?></option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" id="ecategory" name="ecategory" value="110"/>
                                    <input type="hidden" id="bcategory" name="bcategory" value="111"/>

                                    <div class="text_div">
                                        <div class="text_name left colore_12">Subject :</div>
                                        <div class="text_fild left">

                                            <div class="select-style">
                                                <select name="subject" id="subject" onchange="showanswer(this.value)">
                                                    <option value="">Subject</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
                                <form action="" method="" name="answer">
                                    <div class="left height_auto background_14 mcq_answer shawdow_1" id="answer">





                                    </div>
                                </form>

                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>  
                    </div>
                    <!----welcome End here-------------------------------------------->



                </div>
                <!----body_main End here-------------------------------------------->

                <!----fotter start here-------------------------------------------->
                <?php include("include/fotter.php"); ?>
                <!----fotter End here-------------------------------------------->

            </div>

    </body>
</html>
