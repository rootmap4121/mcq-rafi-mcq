<?php 
session_start();
$error_data[]='';
$error_flag=false;
include('class/db_Class_index.php');
$obj=new db_class();
$table='ams_student';
if(isset($_POST['save']))
{
    $exist=array("fname"=>$_POST['fname']);
    $insert=array("fname"=>$_POST['fname'],"lname"=>$_POST['lname'],"user_name"=>$_POST['user'],"pass"=>$_POST['pass']);
    
    if($obj->exists($table,$exist)==1)
    {
        $error_data[]="<div class='error_msg'>Already Exists</div>";
        $error_flag=true;
        
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$error_data;
            session_write_close();
            header('location: singup.php');
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $error_data[]="<div class='sucess'>Successfully Saved</div>";
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$error_data;
             session_write_close();
             header('location: student.php');
             exit();
         }
     }
 else
     {
      $error_data[]='Failed to Save';
      $error_flag=true;
      if($error_flag)
      {
        $_SESSION['ERRMSG_ARR']=$error_data;
        session_write_close();
        header('location:singup.php');
        exit();
      }
     }
    }
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/background.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<body>
<div id="body">
 <?php include("include/index_head_menu.php");?>
<!----body_main start here-------------------------------------------->
   <div id="body_main">
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       <div class="head_logo left">
         <a href="#"><img src="img/logo.png" alt="" /></a>
       </div>
       <div class="head_manu left">
         
         <?php include("include/menu.php");?>
       </div>
       </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----gallery start here-------------------------------------------->
     
     
     <div id="login" class="height_380 background_18">
     
       <div class="head_loging background_14 shawdow_1">
           
              <div class="instrc left background_19">
               Sing Up
               </div>
           
              <div class="instrc right background_19">
                  Instruction
               </div>
           
            </div>
        <form action="" name="singup" method="post" >
        <div class="gallery_singup left">
        <!-------massege Start here------------->
                
                <?php
if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
    foreach($_SESSION['ERRMSG_ARR'] as $msg) 
        {
?>
<span class="label label-warning"><i class="icon-warning-sign bigger-120"></i> <?php echo $msg;  ?> </span>
<?php
        }
    unset($_SESSION['ERRMSG_ARR']);
}
?>
                
           <!-------massege End here------------->
           <ul class="background_14 shawdow_1">
            <li class="left width_130">Name :</li>
            <li class="left">
               <input class="textbox width_80 left " name="fname" value="" placeholder="type fast name" type="text" />
               <input class="textbox width_80 left margin_left_5" name="lname" value="" placeholder="type last name" type="text" />
            </li>
            <li class="left width_130" >User Name :</li>
            <li class="left"><input class="textbox width_205" name="user" value="" placeholder="type user name" type="text" /></li>
            <li class="left width_130">Password :</li>
            <li class="left"><input class="textbox width_205" name="pass" value="" placeholder="type password" type="password" /></li>
            <li class="left width_130">Retype-Password :</li>
            <li class="left"><input class="textbox width_205" name="re-pass" placeholder="type re-password" type="password" /></li>
            <li class="left width_100p text_aling">
               <button name="save" type="submit" class="background_14 colore_11 shawdow_1">submit</button>
            </li>
            
          </ul>
        </div>
        
        <div class="singup_logo right">
            <ul class="background_14 shawdow_1">
            <li>Fast Type Your Name Title and Name</li>
            <li>Type Your User Name</li>
            <li>Type Your Password</li>
            <li>Type Your Re-Type Password</li>
            <img src="img/intr.png" class="margin_left_150 margin_top_10" />
          </ul>
        
          
        </div>
        </form>
       </div>
        
    
     <!----gallery End here-------------------------------------------->
     
     
     
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
<?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
