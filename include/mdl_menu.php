<div id="madl_part_1">
       <h1>Rafi MCQ Programs</h1>
         <div class="siderbar_menu left background_14 shawdow_1">
              <h1>MCQ</h1>
              <ul>
                 <li><a href='mcq.php'>PSC</a></li>
                 <li><a href='class_vi_model_test.php'>Class-VI</a></li>
                 <li><a href='class_vii_model_test.php'>Class-VII</a></li>
                 <li><a href='class_js_model_test.php'>JSC</a></li>
                 <li><a href='class_ix_model_test.php'>Class-IX</a></li>
                 <li><a href='class_ssc_model_test.php'>SSC</a></li>
                 <li><a href='class_hsc_model_test.php'>HSC</a></li>
                 <li><a href='class_lgcse_model_test.php'>IGCSE</a></li>
                 <li><a href='class_a_level_model_test.php'>A Level</a></li>
           </ul>
         </div>
         
         <div class="siderbar_menu background_14 left shawdow_1 margin_left_1">
              <h1>MCQ Testpepar</h1>
              <ul>
               <li><a href="test_paper.php">PSC</a></li>
               <li><a href="test_paper_vi.php">Class-VI</a></li>
               <li><a href="test_paper_vii.php">Class-VII</a></li>
               <li><a href="test_paper_jsc.php">JSC</a></li>
               <li><a href="test_paper_ix.php">Class-XI</a></li>
               <li><a href="test_paper_ssc.php">SSC</a></li>
               <li><a href="test_paper_hsc.php">HSC</a></li>
               <li><a href="test_paper_igcse.php">IGCSE</a></li>
               <li><a href="test_paper_alevel.php">A Level</a></li>
           </ul>
         </div>
         
         <div class="siderbar_menu background_14 left shawdow_1 margin_left_1">
              <h1>MCQ Quiz</h1>
              <ul>
               <li><a href="#">PSC</a></li>
               <li><a href="#">Class-VI</a></li>
               <li><a href="#">Class-VII</a></li>
               <li><a href="#">JSC</a></li>
               <li><a href="#">Class-XI</a></li>
               <li><a href="#">SSC</a></li>
               <li><a href="#">HSC</a></li>
               <li><a href="#">IGCSE</a></li>
               <li><a href="#">A Level</a></li>
               <li><a href="#">University</a></li>
           </ul>
         </div>
         
         <div class="siderbar_menu background_14 left shawdow_1 margin_left_1">
              <h1>MCQ Job</h1>
              <ul>
               <li><a href="#">BCS Preliminary</a></li>
               <li><a href="#">Others</a></li>
           </ul>
         </div>
         
         <div class="siderbar_menu background_14 left shawdow_1 margin_left_1">
              <h1>Composition</h1>
              <ul>
                 <li><a href='Paragraph.php'>Paragraph</a></li>
                 <li><a href='letter.php'>Letter</a></li>
                 <li><a href='apllication.php'>Apllication</a></li>
                 <li><a href='story.php'>Story</a></li>
                 <li><a href='composition.php'>Composition</a></li>
           </ul>
         </div>
         
         <div class="siderbar_menu background_14 right shawdow_1">
              <h1>Video Class</h1>
              <ul>
               <li><a href="vodie_class_psc.php">PSC</a></li>
               <li><a href="vodie_class_vi.php">Class-VI</a></li>
               <li><a href="#">Class-VII</a></li>
               <li><a href="#">JSC</a></li>
               <li><a href="#">Class-XI</a></li>
               <li><a href="#">SSC</a></li>
               <li><a href="#">HSC</a></li>
               <li><a href="#">IGCSE</a></li>
               <li><a href="#">A Level</a></li>
           </ul>
         </div>
       <div class="clear"></div>
     </div>