<div id='cssmenu'>
<ul>
   <li class='active coll '><a href='welcome.php' class="shawdow_1" style="background:#1e559a;color:#FFF;"><span>Home</span></a></li>
   
   <li class='has-sub'><a href='#'><span>MCQ</span></a>
      <ul>
      <?php
	  
	  ?>
         <li><a href='mcq.php'><span>PSC</span></a></li>
         <li><a href='class_vi_model_test.php'><span>Class-VI</span></a></li>
         <li class='last'><a href='class_vii_model_test.php'><span>Class-VII</span></a></li>
         <li class='last'><a href='class_js_model_test.php'><span>JSC</span></a></li>
         <li class='last'><a href='class_ix_model_test.php'><span>Class-IX</span></a></li>
         <li class='last'><a href='class_ssc_model_test.php'><span>SSC</span></a></li>
         <li class='last'><a href='class_hsc_model_test.php'><span>HSC</span></a></li>
         <li class='last'><a href='class_lgcse_model_test.php'><span>IGCSE</span></a></li>
         <li class='last'><a href='class_a_level_model_test.php'><span>A Level</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>MCQ Test Papers</span></a>
      <ul>
         <li><a href='test_paper.php'><span>PSC</span></a></li>
         <li><a href='test_paper_vi.php'><span>Class-VI</span></a></li>
         <li class='last'><a href='test_paper_vii.php'><span>Class-VII</span></a></li>
         <li class='last'><a href='test_paper_jsc.php'><span>JSC</span></a></li>
         <li class='last'><a href='test_paper_ix.php'><span>Class-XI</span></a></li>
         <li class='last'><a href='test_paper_ssc.php'><span>SSC</span></a></li>
         <li class='last'><a href='test_paper_hsc.php'><span>HSC</span></a></li>
         <li class='last'><a href='test_paper_igcse.php'><span>IGCSE</span></a></li>
         <li class='last'><a href='test_paper_alevel.php'><span>A Level</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span class="">MCQ Quiz</span></a>
      <ul>
         <li><a href='mcq_quiz_psc.php'><span>PSC</span></a></li>
         <li><a href='mcq_quiz_class_vi.php'><span>Class-VI</span></a></li>
         <li class='last'><a href='#'><span>Class-VII</span></a></li>
         <li class='last'><a href='#'><span>JSC</span></a></li>
         <li class='last'><a href='#'><span>Class-XI</span></a></li>
         <li class='last'><a href='#'><span>SSC</span></a></li>
         <li class='last'><a href='#'><span>HSC</span></a></li>
         <li class='last'><a href='#'><span>IGCSE</span></a></li>
         <li class='last'><a href='#'><span>A Level</span></a></li>
         <li class='last'><a href='#'><span>University</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>MCQ Job</span></a>
      <ul>
          <li><a href='mcq_job_bcs.php'><span>BCS Preliminary</span></a></li>
         <li><a href='others.php'><span>Others</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Composition</span></a>
      <ul>
         <li><a href='Paragraph.php'><span>Paragraph</span></a></li>
         <li><a href='letter.php'><span>Letter</span></a></li>
         <li class='last'><a href='apllication.php'><span>Apllication</span></a></li>
         <li class='last'><a href='story.php'><span>Story</span></a></li>
         <li class='last'><a href='composition.php'><span>Composition</span></a></li>
      </ul>
   </li>
   <li class='has-sub'><a href='#'><span>Video Class</span></a>
      <ul>
         <li><a href='vodie_class_psc.php'><span>PSC</span></a></li>
         <li><a href='vodie_class_vi.php'><span>Class-VI</span></a></li>
         <li class='last'><a href='#'><span>Class-VII</span></a></li>
         <li class='last'><a href='#'><span>JSC</span></a></li>
         <li class='last'><a href='#'><span>Class-XI</span></a></li>
         <li class='last'><a href='#'><span>SSC</span></a></li>
         <li class='last'><a href='#'><span>HSC</span></a></li>
         <li class='last'><a href='#'><span>IGCSE</span></a></li>
         <li class='last'><a href='#'><span>A Level</span></a></li>
      </ul>
   </li>
</ul>
</div>