<?php 
include('class/auth_index.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link href="css/background.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script>
            function showmcqix(str)
            {
                if (str == "")
                {
                    document.getElementById("ix").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("ix").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/mcq_hsc.php?q="+str, true);
                xmlhttp.send();
            }
</script>

</head>
	
<body>
<div id="body">
  <?php include("include/head_menu.php");?>
<!----body_main start here-------------------------------------------->
   <div id="body_main">
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       
       <div class="head_logo left">
         <img src="img/logo.png" />
       </div>
       <div class="head_manu left">
         <?php include("include/menu.php");?>
       </div>
       
       </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----welcome start here-------------------------------------------->
     <div id="welcom">
       <div class="welcom_admin">
         
           <div class="wlc shawdow_1 background_14">Welcome to Profile 
             <div class="wlc_logo right">
               <ul>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/contact(2).png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/chat.png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/message.png" /></a></li>
               </ul>
             </div> 
            </div>
            
            <div class="body_adin height_300">
            <?php include('include/profile.php');?>
            
           <div class="mcq left">
             <div class="mcq_head background_14 shawdow_1 colore_11">
               PSC Online MCQ Class-IX Model Test
             </div>
            
             <div class="mcq_text_fild margin_top_5 shawdow_2">
               <div class="text_div">
                 <div class="text_name left colore_12">Medium :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select onchange="showmcqix(this.value)">
                        <option value="">Medium</option>
                        <?php 
						  $medium=$obj->SelectAll('ams_madiam');
						  foreach($medium as $rowme):
						?>
                        <option value="<?php echo $rowme->id;?>"><?php echo $rowme->medium_name;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_name left colore_12">Subject :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select name="ix" id="ix">
                        <option value="volvo">Subject</option>
                      </select>
                    </div>
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_name left colore_12">Exam :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select>
                        <option value="volvo">Exam</option>
                        <?php 
						  $ixmcq=$obj->selectAll('ams_exam');
						  foreach($ixmcq as $roix):
						?>
                        <option value="<?php echo $roix->id;?>"><?php echo $roix->exam_name;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_button"><button class="background_14 colore_11 shawdow_1" type="submit" name="go">GO</button></div>
               </div>
             </div></div> </div>
                 <table class="CSSTableGenerator width_100p" >
                    <tr>
                        <td>
                            SL NO
                        </td>
                        <td >
                           Name
                        </td>
                        <td>
                            View
                        </td>
                        <td>
                            Download
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                     <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr> 
                     <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr> 
                     <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr> 
                   
                    
                </table>
            
           
           
           
           <div class="clear"></div>  
       </div>  
     </div>
     <!----welcome End here-------------------------------------------->
     
    
     
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
<?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
