<?php
class db_class {
    
    function company_report_logo_3()
    {
        return "<img src='images/erp.png'>";
    }
    
    function company_report_logo_2()
    {
        return "<h2 align='left' style='margin-top:0; margin-bottom:0;'><img src='images/logo.png' height='60' width='150'></h2>";
    }
    function company_report_logo()
    {
        return "<h2 align='center' style='margin-top:0; margin-bottom:0;'><img src='images/logo.png' height='60' width='170'></h2>";
    }
    
    function company_report_head()
    {
        return "<h4 align='center'><b>Desh Pharmacuticals (pvt) Ltd</b><small><br>2-D/11-C extension pallabi
Mirpur-1216 dhaka,Bangladesh. <br>Phone:, +88-02-8034513 <br>
Fax: +88-02-9010113 </small><h4>";
    }
    
    function company_report_name($name)
    {
        return "<h3 style='text-decoration:none; font-size:15px;' align='center'><b>".$name."</b><h3>";
    }

    public function open() {
        $con = mysqli_connect("localhost", "root", "", "mcq");
        return $con;
    }

    public function close($con) {
        mysqli_close($con);
    }

    
    function getDays($start_date, $end_date) 
    {
        $days[] = $current_date = $start_date;
        while($current_date < $end_date){
            $current_date = date('Y-m-d', strtotime('+1 day', strtotime($current_date)));
            $days[] = $current_date;
        }
        return count($days);
    }
    
    function insert($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        foreach ($object_array as $col => $val) {
            if ($count++ != 0)
                $fields .= ', ';
            $col = mysqli_real_escape_string($con, $col);
            $val = mysqli_real_escape_string($con, $val);
            $fields .= "`$col` = '$val'";
        }
        $query = "INSERT INTO `$object` SET $fields";
        if (mysqli_query($con, $query)) {
            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function make_notification($getidfrom,$detail,$emplid,$supervisor,$status) 
    {
        //setting up
            $notification_array=array("emplid"=>$emplid,
                "f_emplid"=>$supervisor,
                "nid"=>$this->lasgettid($getidfrom),
                "detail"=>$detail,
                "s_status"=>1,
                "a_status"=>1,
                "status"=>$status,
                "date"=>date('Y-m-d'));
        //setting up        
        $sql=$this->insert("notification",$notification_array);
        if ($sql) 
        {
            return 1;
        } 
        else 
        {            
            return 0;
        }
    }
    
    
    
    function totalnotification($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM notification WHERE a_status='1'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return $count;
                $this->close($con);
            }
        }
       
    }
    
    function notification_ind($status) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM notification WHERE status='$status' AND a_status!='2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return $count;
                $this->close($con);
            }
        }
       
    }
    
    function totalnotification_user($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM notification WHERE s_status='1'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return $count;
                $this->close($con);
            }
        }
       
    }
    
    function notification_ind_user($status,$emplid) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM notification WHERE status='$status' AND emplid='$emplid' AND s_status!='2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return $count;
                $this->close($con);
            }
        }
       
    }
    
    function notification_ind_super($status,$emplid) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM notification WHERE status='$status' AND f_emplid='$emplid' AND s_status!='2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return $count;
                $this->close($con);
            }
            else 
            {
                return 0;
                $this->close($con);
            }
        }
       
    }
    
    function notification_check_user($nid,$req_st,$emplid) {
        $count = 0;
        $fields = '';
        $emp_status=$this->SelectAllByVal("employee","id",$emplid,"status");
        
        $con = $this->open();
        if($emp_status==1)
        {
            $query = "SELECT * FROM notification WHERE nid='$nid' AND status='$req_st' AND s_status='1'";
        }
        else 
        {
            $query = "SELECT * FROM notification WHERE nid='$nid' AND status='$req_st' AND a_status='1'";
        }
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                return 1;
                $this->close($con);
            }
            else 
            {
                return 0;
                $this->close($con);                
            }
        }
       
    }
    
    function notification_check_user_update($nid,$req_st,$emplid) {
        $count = 0;
        $fields = '';
        $emp_status=$this->SelectAllByVal("employee","id",$emplid,"status");
        $con = $this->open();
        if($emp_status==1)
        {
            $query = "UPDATE notification SET s_status='2' WHERE nid='$nid' AND status='$req_st'";
        }
        else 
        {
            $query = "UPDATE notification SET a_status='2' WHERE nid='$nid' AND status='$req_st'";
        }
        $result = mysqli_query($con, $query);
        if ($result) {
                return 1;
                $this->close($con);
        }
        else 
        {
            return 0;
            $this->close($con);
        }
       
    } 
    
    
    function lasgettid($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC LIMIT 1";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            if($count!=0)
            {
                $rows=  mysqli_fetch_array($result);
                return $rows['id'];
                $this->close($con);
            }
        }
       
    }
    
    function lastid($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC LIMIT 1";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
       
    }
    
    function redirect($link)
    {
       echo "<script>location.href='$link'</script>";
    }
    function login($username, $password) {
        $count = 0;
        $fields = '';
        $con = $this->open();

        $fields = "username='$username' and password='$password'";

        $query = "SELECT * FROM `admin` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    /**
     * if the object is exists
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function exists($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count == 1) {
                return 1;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function SelectAllByConditionWithAnd($object, $object_array) 
    { 
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            $query = "SELECT * FROM `$object` WHERE $fields"; 
            $result = mysqli_query($con, $query);
            $counts=  mysqli_num_rows($result);
            return $counts;
           
    }
    
    function texists($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function exists_multiple($object, $object_array) {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            $query = "SELECT * FROM `$object` WHERE $fields"; 
            $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count !=0) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function existsnewtotal($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        
        $query = "SELECT * FROM `$object` WHERE ". join(' AND ', $fields);
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count >= 1) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
        function SelectAllByIDDouble($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "SELECT * FROM `$object` WHERE " . join('AND ', $fields) . "";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function existsbyid($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
            if ($count != 0) {
                return $count;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function totalrows($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }
    
        function totalrowsbyID($object,$id) 
        {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE id='$id'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);
                $this->close($con);
                    return $count;
                } else {
                $this->close($con);
                return 0;
            }
        }

            function totalrowsbyDID($object,$field,$id) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field`='$id'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
            $this->close($con);
                return $count;
            } else {
            $this->close($con);
            return 0;
        }
    }
    
    /**
     * Select all the objects
     * @param type $object
     * @return array
     */
    function SelectAll($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object`";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
    function SelectAll_limit_order($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id ASC LIMIT 5";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
        function SelectAll_sdate($object,$field,$date) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field`='$date'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
        
        function SelectAll_ddate($object,$field,$startdate,$enddate) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field` >= '$startdate' AND `$field` <= '$enddate'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
        
        function SelectAll_ddate_ind($object,$field,$startdate,$enddate,$field,$fval) {
            $count = 0;
            $fields = '';
            $con = $this->open();
            $query = "SELECT * FROM `$object` WHERE `$field` >= '$startdate' AND `$field` <= '$enddate' AND `$field`='$fval'";
            $result = mysqli_query($con, $query);
            if ($result) {
                $count = mysqli_num_rows($result);

                if ($count >= 1) {
                    //$object[]=array();
                    while ($rows = $result->fetch_object()) {
                        $objects[] = $rows;
                    }
                    $this->close($con);
                    return $objects;
                }
            }
        }
    
        function SelectAllorderBy($object) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` ORDER BY id DESC";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        }
    }
    
    function title()
    {
        return "<title>Mini ERP System</title>";
    }
    
    function accounts_title()
    {
        return "<title>Accounts Module | Mini ERP</title>";
    }
    
    function accounts_footer()
    {
        return "<div id='footer' align='center'>
            	Copyright AM / AMS IT 2012. All Rights Reserved.
            						</div>";
    }
    
    function softwarename()
    {
        return "<img src='images/logo_1.png' height='40' class='software_name_span_img'><div class='software_name'>Mini ERP System <span class='software_name_span'>Powered By AM/AMS IT</span></div>";
    }
    
    function softwarenames()
    {
        return "<img src='images/logo.png' width='375' height='95' style='background:#fff;'>";
    }
    
    
    function filename()
    {
        return basename($_SERVER['PHP_SELF']);
    }
    
    function leave_status($status)
    {
        if($status==1)
        {
            return "Pending";
        }
        elseif($status==2) 
        {
            return "Acceped By Manager";
        }
        elseif($status==3) 
        {
            return "Rejected By Manager";
        }
    }
    
    function leave_quantity($leave_type)
    {
        $con=$this->open();
        $sql="SELECT * FROM requisation_leaveapplication WHERE year='".date('Y')."' AND leave_type='$leave_type'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                $objects=0;
                    while($rows=mysqli_fetch_array($result)):
                        $objects+=$rows['quantity'];
                    endwhile;
               return $objects;
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }
    
    
    function emp_leave_quantity($emp)
    {
        $con=$this->open();
        $sql="SELECT * FROM requisation_leaveapplication_form WHERE emplid='$emp' AND status='2'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                $objects=0;
                    while($rows=mysqli_fetch_array($result)):
                        $objects+=$rows['datequantity'];
                    endwhile;
               return $objects;
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }
    
    function emp_leave_quantity_new($emp)
    {
        $con=$this->open();
        $sql="SELECT * FROM requisation_leaveapplication_form WHERE emplid='$emp' AND status='1'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                $objects=0;
                    while($rows=mysqli_fetch_array($result)):
                        $objects+=$rows['datequantity'];
                    endwhile;
               return $objects;
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }
    
    
    function emp_leave_status($status)
    {
        if($status==2)
        {
            $msg=" (Approved)";
        }
        else 
        {
            $msg=" (Not Approved)";
        }
        
        return $msg;
    }



    function emp_designation($emp)
    {
        $con=$this->open();
        $sql="SELECT * FROM employee WHERE id='$emp'";
        $result=  mysqli_query($con, $sql);
        if($result)
        {
            $count=  mysqli_num_rows($result);
            if($count!=0)
            {
                    $rows=mysqli_fetch_array($result);
                    $sqldes=  mysqli_query($con,"SELECT * FROM designation WHERE id='".$rows['designation']."'");
                    $fetdes=  mysqli_fetch_array($sqldes);
                    return $fetdes['name'];
            }
            else 
            {
                return 0;
            }
        }
        else 
        {
            return 0;
        }
    }




    /**
     * Select object by ID
     * @param type $object
     * @param type $object_array
     * @return int
     */
    function SelectAllByID($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    function SelectAllByID_Multiple($object, $object_array) 
    {
        $count = 0; 
        $fields = ''; 
        $con = $this->open(); 
            foreach ($object_array as $col => $val) 
            { 
                if ($count++ != 0) 
                $fields .= ' AND '; 
            $col = mysqli_real_escape_string($con, $col); 
            $val = mysqli_real_escape_string($con, $val); 
            $fields .= "`$col` = '$val' "; 
            } 
            
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByID2($object, $object_array,$f,$v) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields AND `$f`='$v'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllByID3($object, $object_array,$f,$v,$f3,$v3) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields AND `$f`='$v' AND `$f3`='$v3'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
            else
            {
                return 0;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function SelectAllNotMe($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` != '$val'";
            }
        }
        $query = "SELECT * FROM `$object` WHERE $fields";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    
    
    function SelectAllByVal($object,$field,$fval,$fetch)
    {
        $link=$this->open();
        $sql="SELECT `$fetch` FROM `$object` WHERE `$field`='$fval'";
        $result=mysqli_query($link, $sql);
        if($result)
        {
            $row=  mysqli_fetch_array($result);
            return $row[$fetch];
			
        }
    }
	
	function SelectAllByVal2($object,$field,$fval,$field2,$fval2,$fetch)
    {
        $link=$this->open();
        $sql="SELECT `$fetch` FROM `$object` WHERE `$field`='$fval' AND `$field2`='$fval2'";
        $result=mysqli_query($link, $sql);
        if($result)
        {
            $row=  mysqli_fetch_array($result);
            return $row[$fetch];
        }
    }
    
    function SelectAllByCountMDI($object,$field1,$val1,$field2,$val2) {
        $count=0;
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field1`='$val1' AND `$field2`='$val2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);
                $this->close($con);
                return $count;
        } else {
            $this->close($con);
            return 0;
        }
    }
    
        function SelectAllByMDI($object,$field1,$val1,$field2,$val2) {
        $count=0;
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE `$field1`='$val1' AND `$field2`='$val2'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = $result->fetch_object()) {
                    $objects[] = $rows;
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }

    /**
     * Delete the object from database
     * @param type $object
     * @param type $object_array
     * @return string|\Exception
     */
    function delete($object, $object_array) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        if (count($object_array) <= 1) {
            foreach ($object_array as $col => $val) {
                if ($count++ != 0)
                    $fields .= ', ';
                $col = mysqli_real_escape_string($con, $col);
                $val = mysqli_real_escape_string($con, $val);
                $fields .= "`$col` = '$val'";
            }
        }
        $query = "Delete FROM `$object` WHERE $fields";
        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Delete the object
     * @param type $object
     * @param type $object_array
     */
    function update($object, $object_array) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET " . join(', ', $fields) . " WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
	
	function update2($object, $object_array,$fid,$val) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET " . join(', ', $fields) . " WHERE $key = '$value' AND `$fid`='$val'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function quantity($object, $object_array,$quantity) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET quantity=quantity+'$quantity' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function quantityd($object, $object_array,$quantity) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET quantity=quantity-'$quantity' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    function amount_incre($object, $object_array,$field,$amount) {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET `$field`=`$field`+'$amount' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) {

            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    
    
    function amount_decre($object, $object_array,$field,$amount) 
    {
        $con_key_from_arr = array_keys($object_array);
        $key = $con_key_from_arr[0];
        $value = array_shift($object_array);
        $fields = array();
        $con = $this->open();
        foreach ($object_array as $field => $val) 
        {
            $fields[] = "$field = '" . mysqli_real_escape_string($con, $val) . "'";
        }
        $query = "UPDATE `$object` SET `$field`=`$field`-'$amount' WHERE $key = '$value'";

        if (mysqli_query($con, $query)) 
        {
            $this->close($con);
            return 1;
        } else {
            return 0;
        }
    }
    

    
    function baseUrl($suffix = '') {
        $protocol = strpos($_SERVER['SERVER_SIGNATURE'], '443') !== false ? 'https://' : 'http://';
        $web_root = $protocol . $_SERVER['HTTP_HOST'] . "/" . "Dropbox/msh/";
        $suffix = ltrim($suffix, '/');
        return $web_root . trim($suffix);
    }

    
    
    function reqstatus($status)
    {
        if($status==1)
        {
            $msg="Pending";
        }
        elseif($status==2)
        {
            $msg="Accepted By Manager";
        }
        elseif($status==3)
        {
            $msg="Complete";
        }
        elseif($status==4)
        {
            $msg="Partial Complete";
        }
        return $msg;
    }
    
    
    
    
    function bodyhead()
    {
        return '<meta charset="utf-8" />'.$this->title().'<meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/bootstrap.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/font-awesome.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace-rtl.min.css").' />
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/ace-skins.min.css").' />
        <script src='.$this->baseUrl("assets/js/ace-extra.min.js").'>
        </script><script src='.$this->baseUrl("assets/print.js").'></script>
        <link rel="stylesheet" href='.$this->baseUrl("assets/css/datepicker.css").' />
	<link rel="stylesheet" href='.$this->baseUrl("assets/css/daterangepicker.css").' />';
    }
    
    function bodyfooter()
    {
        return '<script src='.$this->baseUrl("assets/js/jquery-2.0.3.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.mobile.custom.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/bootstrap.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/typeahead-bs2.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.dataTables.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/jquery.dataTables.bootstrap.js").'></script>
        <script src='.$this->baseUrl("assets/js/ace-elements.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/ace.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/date-time/bootstrap-datepicker.min.js").'></script>
        <script src='.$this->baseUrl("assets/js/date-time/daterangepicker.min.js").'></script>';
    }
    
    
    function clean($str) {
        $str = @trim($str);
        if (get_magic_quotes_gpc()) {
            $str = stripslashes($str);
        }
        return mysql_real_escape_string($str);
    }

    function RandNumber($e) {
        for ($i = 0; $i < $e; $i++) {
            @$rand = $rand . rand(0, 9);
        }
        return $rand;
    }
    
    function empstatus($st)
    {
        if($st==1){ $power="Administrator"; }
        elseif ($st==2){ $power="Manager Access"; }
        else{ $power="Employee"; }
        return $power;
    }
    
    function sex($st)
    {
        if($st==1){ $power="Male"; }
        elseif ($st==2){ $power="Female"; }
        return $power;
    }
    
    function empreq_status($st)
    {
        if($st==1){ $power="Store Requisation"; }
        elseif ($st==2){ $power="Vehicle Request"; }
        elseif ($st==3){ $power="Meeting Reservation Request"; }
        elseif ($st==4){ $power="Leave Request"; }
        return $power;
    }
            
    function dmy($month)
    {
        $chkj = strlen($month);
        if ($chkj == 1) {
           return $chkjval = "0" . $month;
        } 
        else 
        {
           return $chkjval = $month;
        }
    }

    function randomPassword() {
        $alphabet = "EF+GHI234WXYZ567+89@(0-=1){<>/\_+$}[]%$*ABCD";
        $pass = array();
        for ($i = 0; $i < 6; $i++) {
            $n = rand(0, strlen($alphabet) - 1);
            $pass[$i] = $alphabet[$n];
        }
        return implode($pass);
    }

    function cleanQuery($string) {
        if (get_magic_quotes_gpc())
            $string = stripslashes($string);
        return mysql_escape_string($string);
    }

    function limit_words($string, $word_limit) {
        $words = explode(" ", $string);
        return implode(" ", array_splice($words, 0, $word_limit)) . "...";
    }
    
    function today() {
        return date('Y-m-d');
    }

    function bn_date($str) {
        $en = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);
        $bn = array('১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০');
        $str = str_replace($en, $bn, $str);
        $en = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        $en_short = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
        $bn = array('জানুয়ারী', 'ফেব্রুয়ারী', 'মার্চ', 'এপ্রিল', 'মে', 'জুন', 'জুলাই', 'অগাস্ট', 'সেপ্টেম্বর', 'অক্টোবর', 'নভেম্বর', 'ডিসেম্বর');
        $str = str_replace($en, $bn, $str);
        $str = str_replace($en_short, $bn, $str);
        $en = array('Saturday', 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday');
        $en_short = array('Sat', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri');
        $bn_short = array('শনি', 'রবি', 'সোম', 'মঙ্গল', 'বুধ', 'বৃহঃ', 'শুক্র');
        $bn = array('শনিবার', 'রবিবার', 'সোমবার', 'মঙ্গলবার', 'বুধবার', 'বৃহস্পতিবার', 'শুক্রবার');
        $str = str_replace($en, $bn, $str);
        $str = str_replace($en_short, $bn, $str);
        $en = array('am', 'pm');
        $bn = array('পূর্বাহ্ন', 'অপরাহ্ন');
        $str = str_replace($en, $bn, $str);
        return $str;
    }

    function getExtension($str) {
        $i = strrpos($str, ".");
        if (!$i) {
            return "";
        }
        $l = strlen($str) - $i;
        $ext = substr($str, $i + 1, $l);
        return $ext;
    }

    function thumb($width, $height, $destination) 
    {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $ratio = max($width / $w, $height / $h);
        $h = ceil($height / $ratio);
        $x = ($w - $width / $ratio) / 2;
        $w = ceil($width / $ratio);
        $path = $destination . 'small_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);
        imagecopyresampled($tmp, $image, 0, 0, $x, 0, $width, $height, $w, $h);
        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }

    function image_caption($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . 'thumb_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);
        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);
        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }

    function image_bigcaption($width, $height, $destination) {
        list($w, $h) = getimagesize($_FILES['image']['tmp_name']);
        $path = $destination . 'big_' . time() . '_' . $_FILES['image']['name'];
        $imgString = file_get_contents($_FILES['image']['tmp_name']);
        $image = imagecreatefromstring($imgString);
        $tmp = imagecreatetruecolor($width, $height);

        imagecopyresized($tmp, $image, 0, 0, 0, 0, $width, $height, $w, $h);

        switch ($_FILES['image']['type']) {
            case 'image/jpeg':
                imagejpeg($tmp, $path, 100);
                break;
            case 'image/png':
                imagepng($tmp, $path, 0);
                break;
            case 'image/gif':
                imagegif($tmp, $path);
                break;
            default:
                exit;
                break;
        }
        return $path;
        imagedestroy($image);
        imagedestroy($tmp);
    }
    
    function backup_tables($host,$user,$pass,$name,$tables = '*')
    {

        $link = mysql_connect($host,$user,$pass);
        mysql_select_db($name,$link);
        mysql_query("SET NAMES 'utf8'");

        //get all of the tables
        if($tables == '*')
        {
            $tables = array();
            $result = mysql_query('SHOW TABLES');
            while($row = mysql_fetch_row($result))
            {
                $tables[] = $row[0];
            }
        }
        else
        {
            $tables = is_array($tables) ? $tables : explode(',',$tables);
        }
        $return='';
        //cycle through
        foreach($tables as $table)
        {
            $result = mysql_query("SELECT * FROM `$table`");
            $num_fields = mysql_num_fields($result);

            $return.= 'DROP TABLE '.$table.';';
            $row2 = mysql_fetch_row(mysql_query("SHOW CREATE TABLE `$table`"));
            $return.= "\n\n".$row2[1].";\n\n";

            for ($i = 0; $i < $num_fields; $i++) 
            {
                while($row = mysql_fetch_row($result))
                {
                    $return.= 'INSERT INTO `$table` VALUES(';
                    for($j=0; $j<$num_fields; $j++) 
                    {
                        $row[$j] = addslashes($row[$j]);
                        $row[$j] = str_replace("\n","\\n",$row[$j]);
                        if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                        if ($j<($num_fields-1)) { $return.= ','; }
                    }
                    $return.= ");\n";
                }
            }
            $return.="\n\n\n";
        }

        //save file
        $namesbackup=date('d_M_Y_H_i_s');
        $handle = fopen('db_backup/'.$namesbackup.'inventory_backup_db.sql','w+');
        fwrite($handle,$return);
        fclose($handle);
        return 1;
    }
    
    function holidays($year) {
        $count = 0;
        $fields = '';
        $con = $this->open();
        $query = "SELECT * FROM `$object` WHERE year='$year'";
        $result = mysqli_query($con, $query);
        if ($result) {
            $count = mysqli_num_rows($result);

            if ($count >= 1) {
                //$object[]=array();
                while ($rows = mysqli_fetch_array($result)) {
                    $objects[] = $rows['dates'];
                }
                $this->close($con);
                return $objects;
            }
        } else {
            $this->close($con);
            return 0;
        }
    }
    
    function getWorkingDays($startDate,$endDate,$holidays){
        // do strtotime calculations just once
        $endDate = strtotime($endDate);
        $startDate = strtotime($startDate);
        
        

        //The total number of days between the two dates. We compute the no. of seconds and divide it to 60*60*24
        //We add one to inlude both dates in the interval.
        $days = ($endDate - $startDate) / 86400 + 1;

        $no_full_weeks = floor($days / 7);
        $no_remaining_days = fmod($days, 7);

        //It will return 1 if it's Monday,.. ,7 for Sunday
        $the_first_day_of_week = date("N", $startDate);
        $the_last_day_of_week = date("N", $endDate);

        //---->The two can be equal in leap years when february has 29 days, the equal sign is added here
        //In the first case the whole interval is within a week, in the second case the interval falls in two weeks.
        if ($the_first_day_of_week <= $the_last_day_of_week) {
            if ($the_first_day_of_week <= 6 && 6 <= $the_last_day_of_week) $no_remaining_days--;
            if ($the_first_day_of_week <= 7 && 7 <= $the_last_day_of_week) $no_remaining_days--;
        }
        else {
            // (edit by Tokes to fix an edge case where the start day was a Sunday
            // and the end day was NOT a Saturday)

            // the day of the week for start is later than the day of the week for end
            if ($the_first_day_of_week == 7) {
                // if the start date is a Sunday, then we definitely subtract 1 day
                $no_remaining_days--;

                if ($the_last_day_of_week == 6) {
                    // if the end date is a Saturday, then we subtract another day
                    $no_remaining_days--;
                }
            }
            else {
                // the start date was a Saturday (or earlier), and the end date was (Mon..Fri)
                // so we skip an entire weekend and subtract 2 days
                $no_remaining_days -= 2;
            }
        }

        //The no. of business days is: (number of weeks between the two dates) * (5 working days) + the remainder
    //---->february in none leap years gave a remainder of 0 but still calculated weekends between first and last day, this is one way to fix it
       $workingDays = $no_full_weeks * 5;
        if ($no_remaining_days > 0 )
        {
          $workingDays += $no_remaining_days;
        }

        //We subtract the holidays
        foreach($holidays as $holiday){
            $time_stamp=strtotime($holiday);
            //If the holiday doesn't fall in weekend
            if ($startDate <= $time_stamp && $time_stamp <= $endDate && date("N",$time_stamp) != 6 && date("N",$time_stamp) != 7)
                $workingDays--;
        }

        return $workingDays;
    }
    
}
?>
