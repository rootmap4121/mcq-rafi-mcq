<?php
include('../class/auth.php');
$table='ams_susetion';
if(isset($_POST['save']))
{
    $exist=array("sus_name"=>$_POST['susetion']);
    $insert=array("sus_name"=>$_POST['susetion'],"madiam_id"=>$_POST['suse_medium'],"class"=>$_POST['suse_class'],"sub_id"=>$_POST['suse_subject'],"sus_date"=>date('y-m-d'),"status"=>1);
    
    if($obj->exists($table,$exist)==1)
    {
        $errmsg_arr[]='Already Exists';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $errmsg_arr[]='Successfully Saved';
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$errmsg_arr;
             session_write_close();
             header('location:'.$obj->filename());
             exit();
         }
     }
 else
    {
      $errmsg_arr[]='Failed to save';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
    }
}
if(isset($_POST['edit']))
{
    $edit=array("id"=>$_POST['id'],"sus_name"=>$_POST['susetion'],"madiam_id"=>$_POST['suse_medium'],"class"=>$_POST['suse_class'],"sub_id"=>$_POST['suse_subject'],"sus_date"=>date('y-m-d'),"status"=>1);
    if($obj->update($table,$edit)==1)
    {
        $errmsg_arr[]='Successfully update'.$_POST['id'];
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
      $errmsg_arr[]='Update Failed';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
}
if(@$_GET['action']== 'delete')
{
    $delet=array("id"=>$_GET['id']);
    if($obj->delete($table,$delet)==1)
    {
        $errmsg_arr[]='Successfully Deleted';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     $errmsg_arr[]='Delete failed';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }   
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Susetion - MCQ Admin</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <script src="assets/js/ace-extra.min.js"></script>

</head>

<body>
<?php include("include_admin/head.php");?>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

 <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
        </a>

                <?php include("include_admin/side_manu.php");?>

                <div class="main-content">
					
                <?php include("include_admin/other_home.php");?>

<div class="page-content">
        <div class="row">
<!----------------------widget start here--------------------------------------------------> 
<div class="col-sm-6">
        <div class="widget-box">
             <div class="widget-header">
                   <h4>Add Susetion</h4>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                            <?php include ('../class/esm.php');?>
                            <form method="post" action="" name="susetion">
                                       
                                        
                                        
                                       <fieldset></fieldset>
                                       
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Susetion Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="susetion" id="form-field-1" placeholder="Test susetion name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Medium Name </label>

                                                <div class="col-sm-9">
                                                    <select name="suse_medium" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                        <option value="">Medium</option>
                                                        <?php 
                                                         $ins=$obj->selectAll('ams_madiam');
                                                         foreach ($ins as $row):
                                                        ?>
                                                        
                                                        <option value="<?php echo $row->id;?>"><?php echo $row->medium_name;?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Class Name </label>

                                                <div class="col-sm-9">
                                                    <select name="suse_class" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                        <option value="">Class</option>
                                                        <?php 
                                                         $ins=$obj->selectAll('ams_class');
                                                         foreach ($ins as $row):
                                                        ?>
                                                        
                                                        <option value="<?php echo $row->id;?>"><?php echo $row->class_name;?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Subject Name </label>

                                                <div class="col-sm-9">
                                                    <select name="suse_subject" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                        <option value="">Subject</option>
                                                        <?php 
                                                         $ins=$obj->selectAll('ams_subject');
                                                         foreach ($ins as $row):
                                                        ?>
                                                        
                                                        <option value="<?php echo $row->id;?>"><?php echo $row->sub_name;?></option>
                                                        <?php endforeach;?>
                                                    </select>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                          
                                        <div class="form-actions center">
                                            <button type="submit" name="save" class="btn btn-sm btn-success">
                                                        Submit
                                                </button>
                                                
                                            <button type="reset" class="btn btn-sm btn-success">
                                                        Reset
                                                </button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
</div>
 <!----------------------widget end here-------------------------------------------------->   
 

  <div class="col-xs-12 col-sm-6 widget-container-span">
<div class="widget-box">
    <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Susetion
            </h5>
    </div>

        <div class="widget-body">
                <div class="widget-main no-padding">
                <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
                <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                                <tr>
                                        <th class="center">
                                            SL no
                                        </th>
                                        
                                        <th>Name</th>
                                        
                                        <th>Medium Name</th>
                                        
                                        <th>Class Name</th>
                                        
                                        <th>Subject Name</th>
                                        
                                        <th class="hidden-480">Status</th>

                                        
                                        
                                        <th class="hidden-480">Edit</th>

                                        <th>Delete</th>
                                </tr>
                        </thead>
                         <?php 
                          $susetion=$obj->selectAll($table);
                          $s=1;
                          foreach ($susetion as $row):
                         ?>
                        <tbody>

                                <tr>
                                        <td class="center">
                                           <?php echo $s;?>
                                        </td>

                                        <td>
                                           <a href="#"><?php echo $row->sus_name;?></a>
                                        </td>
                                        
                                        <td class="hidden-480">
                                            <?php echo $obj->SelectAllByVal("ams_madiam","id",$row->madiam_id,"medium_name"); ?>
                                        </td>
                                        
                                        <td class="hidden-480">
                                            <?php echo $obj->SelectAllByVal("ams_class","id",$row->class,"class_name"); ?>
                                        </td>
                                        
                                        <td class="hidden-480">
                                            <?php echo $obj->SelectAllByVal("ams_subject","id",$row->sub_id,"sub_name"); ?>
                                        </td>

                                        <td class="hidden-480">
                                           <span class="label label-sm label-success"><?php echo $row->status;?></span>
                                        </td>
                                        
                                        
                                        
                                        <td>
                                         <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                       
                                            <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info">
                                                    <i class="icon-edit bigger-120"></i>
                                            </a>
                                             <div id="modal-table<?php echo $row->id;?>" class="modal fade" tabindex="-1">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header no-padding">
                                                            <div class="table-header">
                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                    <span class="white">&times;</span>
                                                                </button>
                                                                Edit Detail :
                                                            </div>
                                                        </div>

                                                        <div class="modal-body no-padding">
                                                         <div class="widget-body">
                                                            <div class="widget-main no-padding">
                                                                
                                                                <form method="post" action="" name="susetion">



                                                                           <fieldset></fieldset>

                                                                            <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Susetion Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <input name="id" type="hidden" value="<?php echo $row->id;?>"/>
                                                                                        <input value="<?php echo $row->sus_name;?>" type="text" name="susetion" id="form-field-1" placeholder="Test susetion name" class="col-xs-10 col-sm-10" />
                                                                                    </div>
                                                                            </div>
                                                                           <fieldset></fieldset>

                                                                           <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Medium Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="suse_medium" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Medium</option>
                                                                                            <?php 
                                                                                             $ins=$obj->selectAll('ams_madiam');
                                                                                             foreach ($ins as $rowd):
                                                                                            ?>

                                                                                            <option value="<?php echo $rowd->id;?>"><?php echo $rowd->medium_name;?></option>
                                                                                            <?php endforeach;?>
                                                                                        </select>
                                                                                    </div>
                                                                            </div>
                                                                           <fieldset></fieldset>

                                                                           <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Class Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="suse_class" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Class</option>
                                                                                            <?php 
                                                                                             $ins=$obj->selectAll('ams_class');
                                                                                             foreach ($ins as $roww):
                                                                                            ?>

                                                                                            <option value="<?php echo $roww->id;?>"><?php echo $roww->class_name;?></option>
                                                                                            <?php endforeach;?>
                                                                                        </select>
                                                                                    </div>
                                                                            </div>
                                                                           <fieldset></fieldset>

                                                                           <div class="form-group">
                                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Subject Name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="suse_subject" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Subject</option>
                                                                                            <?php 
                                                                                             $ins=$obj->selectAll('ams_subject');
                                                                                             foreach ($ins as $rowr):
                                                                                            ?>

                                                                                            <option value="<?php echo $rowr->id;?>"><?php echo $rowr->sub_name;?></option>
                                                                                            <?php endforeach;?>
                                                                                        </select>
                                                                                    </div>
                                                                            </div>
                                                                           <fieldset></fieldset>

                                                                            <div class="form-actions center">
                                                                                <button type="submit" name="edit" class="btn btn-sm btn-success">
                                                                                            Submit
                                                                                    </button>

                                                                                <button type="reset" name="reset" class="btn btn-sm btn-success">
                                                                                            Reset
                                                                                    </button>
                                                                            </div>
                                                                    </form>
                                                            </div>
                                                    </div>
                                                                                            </div>

                                                        <div class="modal-footer no-margin-top">
                                                            <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                                                <i class="icon-remove"></i>
                                                                Close
                                                            </button>
                                                        </div>
                                                    </div><!-- /.modal-content -->
                                                </div><!-- /.modal-dialog -->
                                            </div><!-- PAGE CONTENT ENDS -->



                                         </div>
                                        </td>

                                        <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">

                                            <a href="<?php echo $obj->filename();?>?id=<?php echo $row->id;?>&AMP;action=delete" class="btn btn-xs btn-danger">
                                                    <i class="icon-trash bigger-120"></i>
                                                 </a>
                                         </div>
                                </td>
                        </tr>


                </tbody>
                       <?php $s++;  endforeach;?>
                   </table>
                </div><!-- /.table-responsive -->
        </div><!-- /span -->
</div><!-- /row -->
                </div>
        </div>
</div>
</div>
 
 


<div class="col-xs-12">
    
</div><!-- /.col -->
</div>
</div>
</div>

<?php include("include_admin/left_select.php");?>

   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>
</div>

<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>



<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

		
	</body>


</html>
