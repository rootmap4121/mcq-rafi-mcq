<?php
include("../class/auth.php");
$table='ams_notic';
if(isset($_POST['submit']))
{
    $exit=array ("notic_name"=>$_POST['notic']);
    $insert=array ("notic_name"=>$_POST['notic'],"details"=>$_POST['details'],"notic_date"=>date('y-m-d'),"status"=>1);
    
    if($obj->exists($table,$exit)==1)
    {
        $errmsg_arr[]='Already Exists';
        $error_flag=true;
        
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $errmsg_arr[]='Successfully Saved';
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$errmsg_arr;
             session_write_close();
             header('location:'.$obj->filename());
             exit();
         }
     }
 else
     {
      $errmsg_arr[]='Failed to saved';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
     }
    }
}
if(isset($_POST['edite']))
{
    $edit=array ("id"=>$_POST['id'],"notic_name"=>$_POST['notic'],"details"=>$_POST['details'],"notic_date"=>date('y-m-d'),"status"=>1);
    if($obj->update($table,$edit)==1)
    {
        $errmsg_arr[]='Successfully update'.$_POST['id'];
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
      $errmsg_arr[]='Update Failed';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
}
if(@$_GET['action']== 'delete')
{
    $delet=array("id"=>$_GET['id']);
    if($obj->delete($table,$delet)==1)
    {
        $errmsg_arr[]='Successfully Deleted';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     $errmsg_arr[]='Delete failed';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }   
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Notice - MCQ Admin</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <script src="assets/js/ace-extra.min.js"></script>

</head>

<body>
<?php include("include_admin/head.php");?>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

 <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
        </a>

                <?php include("include_admin/side_manu.php");?>

                <div class="main-content">
					
                <?php include("include_admin/other_home.php");?>

<div class="page-content">
        <div class="row">
<!----------------------widget start here--------------------------------------------------> 
<div class="col-sm-6">
        <div class="widget-box">
             <div class="widget-header">
                   <h4>Add Notic</h4>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                            <?php include ('../class/esm.php');?>
                            <form method="post" action="" name="notc">
                                       
                                        
                                        
                                       <fieldset></fieldset>
                                       
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Notice Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="notic" id="form-field-1" placeholder="type notic name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Text  </label>

                                                <div class="col-sm-9">
                                                    <textarea name="details" class="col-xs-10 col-sm-10" placeholder="type your notice">
                                                        
                                                    </textarea>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                          
                                        <div class="form-actions center">
                                            <button type="submit" name="submit" class="btn btn-sm btn-success">
                                                        Submit
                                                </button>
                                                
                                            <button type="reset" class="btn btn-sm btn-success">
                                                        Reset
                                                </button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
</div>
 <!----------------------widget end here-------------------------------------------------->   
 

  <div class="col-xs-12 col-sm-6 widget-container-span">
<div class="widget-box">
    <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Notice
            </h5>
    </div>

        <div class="widget-body">
                <div class="widget-main no-padding">
                <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
                <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                                <tr>
                                        <th class="center">
                                                <label>
                                                        <span class="lbl">SL no</span>
                                                </label>
                                        </th>
                                        <th>Name</th>
                                        
                                        <th class="hidden-480">Details</th>
                                        
                                        <th>
                                           <i class="icon-time bigger-110 hidden-480"></i>
                                            Date
                                        </th>
                                        
                                        <th class="hidden-480">
                                            Status
                                        </th>
                                        
                                       <th>
                                                Edit
                                        </th>
                                        <th class="hidden-480">Delete</th>
                                </tr>
                        </thead>
                        <?php 
                         $notc=$obj->selectAll($table);
                         $s=1;
                         foreach ($notc as $rowe):
                        ?>
                        <tbody>

                    <tr>
                            <td class="center">
                                <label>
                                       <span class="lbl"><?php echo $s;?></span>
                                    </label>
                            </td>

                            <td>
                                    <a href="#"><?php echo $rowe->notic_name;?></a>
                            </td>
                            
                            <td class="hidden-480">
                                    <?php echo $rowe->details;?>
                            </td>

                            <td><?php echo $rowe->notic_date;?></td>

                            <td class="hidden-480">
                                    <span class="label label-sm label-warning"><?php echo $rowe->status;?></span>
                            </td>

                            <td class="hidden-480">
                               <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">

                                   <a href="#modal-table<?php echo $rowe->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info">
                                            <i class="icon-edit bigger-120"></i>
                                    </a>
                                   <!--edit start here------------------------>    
                                      <div id="modal-table<?php echo $rowe->id;?>" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header no-padding">
                                                                <div class="table-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        <span class="white">&times;</span>
                                                                    </button>
                                                                    Edit Detail :
                                                                </div>
                                                            </div>

                                                            <div class="modal-body no-padding">
                                                              <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                             
                                                                            <form method="post" action="" name="edite">



                                                                                       <fieldset></fieldset>

                                                                                        <div class="form-group">
                                                                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Notice Name </label>

                                                                                                <div class="col-sm-9">
                                                                                                    <input name="id" value="<?php echo $rowe->id;?>" type="hidden"/>
                                                                                                    <input type="text" value="<?php echo $rowe->notic_name;?>" name="notic" id="form-field-1" placeholder="Test paper name" class="col-xs-10 col-sm-10" />
                                                                                                </div>
                                                                                        </div>
                                                                                       <fieldset></fieldset>

                                                                                        <div class="form-actions center">
                                                                                            <button type="submit" name="edite" class="btn btn-sm btn-success">
                                                                                                        Submit
                                                                                                </button>

                                                                                            <button type="reset" class="btn btn-sm btn-success">
                                                                                                        Reset
                                                                                                </button>
                                                                                        </div>
                                                                                </form>
                                                                        </div>
                                                                </div>
                                                            </div>

                                                            <div class="modal-footer no-margin-top">
                                                                <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                                                    <i class="icon-remove"></i>
                                                                    Close
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                    <!--edit end here------------------------>    
                               </div>
                            </td>

                            <td>
                                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                    <a href="<?php echo $obj->filename();?>?id=<?php echo $rowe->id;?>&AMP;action=delete" class="btn btn-xs btn-danger">
                                                <i class="icon-trash bigger-120"></i>
                                        </a>

                                </div>
                            </td>
                    </tr>

                </tbody>
                     <?php $s++;  endforeach;?>  
                   </table>
                </div><!-- /.table-responsive -->
        </div><!-- /span -->
</div><!-- /row -->
    </div>
 </div>
</div>
</div>
 
 


<div class="col-xs-12">
    
</div><!-- /.col -->
</div>
</div>
</div>

<?php include("include_admin/left_select.php");?>

   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>
</div>

<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>



<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

		
	</body>


</html>
