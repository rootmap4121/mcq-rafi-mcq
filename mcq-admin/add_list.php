<?php
include ('../class/auth.php');
$table="ams_add";
if(isset($_POST['edite']))
{
    $edit=array("id"=>$_POST['id'],"name"=>$_POST['name']);
    if($obj->update($table,$edit)==1)
    {
        $errmsg_arr[]='Successfully update'.$_POST['id'];
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
      $errmsg_arr[]='Update Failed';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
}
if(@$_GET['action']== 'delete')
{
    $delet=array("id"=>$_GET['id']);
    if($obj->delete($table,$delet)==1)
    {
        $errmsg_arr[]='Successfully Deleted';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     $errmsg_arr[]='Delete failed';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }   
    }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Class - MCQ Admin</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <script src="assets/js/ace-extra.min.js"></script>
    
</head>

<body>
<?php include("include_admin/head.php");?>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

 <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
        </a>

                <?php include("include_admin/side_manu.php");?>

                <div class="main-content">
					
                <?php include("include_admin/other_home.php");?>

<div class="page-content">
        <div class="row">
<!----------------------widget start here--------------------------------------------------> 
<?php include ('../class/esm.php');?>
  <div class="col-xs-12 col-sm-12 widget-container-span">
<div class="widget-box">
    <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Answer
            </h5>
    </div>

        <div class="widget-body">
                <div class="widget-main no-padding">
                <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
                <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                                <tr>
                                        <th class="center">
                                          Sl no.
                                        </th>
                                        <th>User Name</th>
                                        
                                        <th>Company Name</th>
                                        
                                        <th>Address</th>
                                        
                                        <th>Image</th>
                                        
                                        <th>
                                                <i class="icon-time bigger-110 hidden-480"></i>
                                                Date
                                        </th>

                                        <th>Edit</th>
                                        
                                        <th>Delete</th>
                                </tr>
                        </thead>
                      <?php 
                      $add=$obj->selectAll($table);
                      $x=1;
                      if(!empty($add))
                      foreach ($add as $addd):
                      ?>
                        <tbody>

                                <tr>
                                        <td class="center">
                                          <?php echo $x;?>     
                                        </td>

                                        <td>
                                          <?php echo $addd->name;?>
                                        </td>
                                        
                                        <td>
                                          <?php echo $addd->company_name;?>
                                        </td>
                                        
                                        <td><?php echo $addd->address;?></td>
                                        
                                        <td>
                                            <img src="../photo/<?php echo $addd->photo;?>" width="50" height="50" />
                                        </td>
                                        
                                        <td class="hidden-480">
                                            <?php echo $addd->date;?>
                                        </td>

                                        <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">                                                       
                                            <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info">
                                                <i class="icon-edit bigger-120"></i>
                                        </a>

                                          <div id="modal-table<?php echo $row->id;?>" class="modal fade" tabindex="-1">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header no-padding">
                                                        <div class="table-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                <span class="white">&times;</span>
                                                            </button>
                                                            Edit Detail :
                                                        </div>
                                                    </div>
                                                       <div class="modal-body no-padding">
                                                       <div class="widget-body">
                                                                <div class="widget-main no-padding">
                                                                   
                                                                    <form method="post" action="" name="add_here">



                                                                               <fieldset></fieldset>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Name </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="hidden" name="id" value="<?php echo $addd->id;?>"/>
                                                                                            <input type="text" value="<?php echo $addd->name;?>" name="name" id="form-field-1" placeholder="name" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               
                                                                               
                                                                               <fieldset></fieldset>

                                                                                <div class="form-actions center">
                                                                                    <button type="submit" name="edite" class="btn btn-sm btn-success">
                                                                                                Submit
                                                                                        </button>

                                                                                    <button type="reset" class="btn btn-sm btn-success">
                                                                                                Reset
                                                                                        </button>
                                                                                </div>
                                                                        </form>
                                                                </div>
                                                        </div>
                                                    </div>

                                                    <div class="modal-footer no-margin-top">
                                                        <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                                            <i class="icon-remove"></i>
                                                            Close
                                                        </button>
                                                    </div>
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- PAGE CONTENT ENDS -->

                                            
                                        </div>

                                        </td>
                                        
                                        <td>
                                        <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">

                                            <a href="<?php echo $obj->filename();?>?id=<?php echo $addd->id;?>&AMP;action=delete" class="btn btn-xs btn-danger">
                                                        <i class="icon-trash bigger-120"></i>
                                                </a>

                                        </div>

                                        </td>
                                </tr>

                         
                </tbody>
                       <?php $x++; endforeach; ?>
                   </table>
                </div><!-- /.table-responsive -->
        </div><!-- /span -->
</div><!-- /row -->
                </div>
        </div>
</div>
</div>
 <!----------------------widget end here-------------------------------------------------->   
 


 
 


<div class="col-xs-12">
    
</div><!-- /.col -->
</div>
</div>
</div>

<?php include("include_admin/left_select.php");?>

   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>
</div>

<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>



<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

		
	</body>


</html>
