<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Gallery - MCQ Admin</title>
        <meta name="description" content="" />
        <meta name="description" content="responsive photo gallery using colorbox" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/colorbox.css" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <script src="assets/js/ace-extra.min.js"></script>

    </head>

    <body>
<?php include("include_admin/head.php"); ?>

        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>

            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>

<?php include("include_admin/side_manu.php"); ?>

                <div class="main-content">

<?php include("include_admin/other_home.php"); ?>

<div class="page-content">
    <div class="row">
        <!----------------------widget start here--------------------------------------------------> 

        <!----------------------widget end here-------------------------------------------------->   

            <div class="col-xs-12">
              <div class="row">
                        <div class="col-xs-12">
                                <!-- PAGE CONTENT BEGINS -->

                                <div class="row-fluid">
                                        <ul class="ace-thumbnails">
                                    <li>
                                            <a href="assets/images/gallery/image-4.jpg" data-rel="colorbox">
                                                    <img alt="150x150" src="assets/images/gallery/thumb-4.jpg" />
                                                    <div class="tags">
                                                            <span class="label-holder">
                                                                    <span class="label label-info arrowed">fountain</span>
                                                            </span>

                                                            <span class="label-holder">
                                                                    <span class="label label-danger">recreation</span>
                                                            </span>
                                                    </div>
                                            </a>

                                            <div class="tools tools-top">
                                                    <a href="#">
                                                            <i class="icon-link"></i>
                                                    </a>

                                                    <a href="#">
                                                            <i class="icon-paper-clip"></i>
                                                    </a>

                                                    <a href="#">
                                                            <i class="icon-pencil"></i>
                                                    </a>

                                                    <a href="#">
                                                            <i class="icon-remove red"></i>
                                                    </a>
                                            </div>
                                    </li>


                                        </ul>
                                </div><!-- PAGE CONTENT ENDS -->
                        </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.col -->
        </div>
    </div>
</div>

<?php include("include_admin/left_select.php"); ?>

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div>

            <script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



            <script type="text/javascript">
                window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
            </script>



            <script type="text/javascript">
                if ("ontouchend" in document)
                    document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");</script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/typeahead-bs2.min.js"></script>

            <script src="assets/js/jquery.colorbox-min.js"></script>

            <script src="assets/js/ace-elements.min.js"></script>
            <script src="assets/js/ace.min.js"></script>
            
            <script type="text/javascript">
			jQuery(function($) {
	var colorbox_params = {
		reposition:true,
		scalePhotos:true,
		scrolling:false,
		previous:'<i class="icon-arrow-left"></i>',
		next:'<i class="icon-arrow-right"></i>',
		close:'&times;',
		current:'{current} of {total}',
		maxWidth:'100%',
		maxHeight:'100%',
		onOpen:function(){
			document.body.style.overflow = 'hidden';
		},
		onClosed:function(){
			document.body.style.overflow = 'auto';
		},
		onComplete:function(){
			$.colorbox.resize();
		}
	};

	$('.ace-thumbnails [data-rel="colorbox"]').colorbox(colorbox_params);
	$("#cboxLoadingGraphic").append("<i class='icon-spinner orange'></i>");//let's add a custom loading icon

	/**$(window).on('resize.colorbox', function() {
		try {
			//this function has been changed in recent versions of colorbox, so it won't work
			$.fn.colorbox.load();//to redraw the current frame
		} catch(e){}
	});*/
})
		</script>


    </body>


</html>
