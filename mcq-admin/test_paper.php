<?php 
include('../class/auth.php');
$table="ams_mcq_testpaper_data";
if(isset($_POST['submit']))
{
    $exist=array("name"=>$_POST['name']);
    $insert=array("name"=>$_POST['name'],"medium_id"=>$_POST['medium'],"class_id"=>$_POST['class'],"detail"=>$_POST['details'],"category_id"=>$_POST['category'],"board"=>$_POST['board'],"year_date"=>$_POST['year'],"date"=>date('Y-m-d'),"status"=>1);
    if($obj->exists($table,$exist)==1)
    {
        $errmsg_arr[]='Already Exists';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
    else 
    {
     
        if($obj->insert($table,$insert)==1)
        {
            $errmsg_arr[]='Successfully Saved';
            $error_flag=true;
            if($error_flag)
            {
                $_SESSION['SMSG_ARR']=$errmsg_arr;
                session_write_close();
                header('location:'.$obj->filename());
                exit();
            }
        }
        else 
        {
            $errmsg_arr[]='Successfully Saved';
            $error_flag=true;
            if($error_flag)
            {
                $_SESSION['SMSG_ARR']=$errmsg_arr;
                session_write_close();
                header('location:'.$obj->filename());
                exit();
            }
        }
        
        
    }
}
if(isset($_POST['edit']))
{
    $edit=array("id"=>$_POST['id'],"name"=>$_POST['paper'],"date"=>date('Y-m-d'),"status"=>1);
    if($obj->update($table,$edit)==1)
    {
        $errmsg_arr[]='Successfully update'.$_POST['id'];
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
      $errmsg_arr[]='Update Failed';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
}
if(@$_GET['action']== 'delete')
{
    $delet=array("id"=>$_GET['id']);
    if($obj->delete($table,$delet)==1)
    {
        $errmsg_arr[]='Successfully Deleted';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     $errmsg_arr[]='Delete failed';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }   
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Test Paper - Ace Admin</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <script src="assets/js/ace-extra.min.js"></script>
    <script>
            function showclass(str)
            {
                if (str == "")
                {
                    document.getElementById("class").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("class").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/board_medium.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function showcategory(str)
            {
                if (str == "")
                {
                    document.getElementById("board").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("board").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/board.php?q="+str, true);
                xmlhttp.send();
            }
</script>
        
</head>

<body>
<?php include("include_admin/head.php");?>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

 <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
        </a>

                <?php include("include_admin/side_manu.php");?>

                <div class="main-content">
					
                <?php include("include_admin/other_home.php");?>

<div class="page-content">
        <div class="row">
<!----------------------widget start here--------------------------------------------------> 
<div class="col-sm-6">
        <div class="widget-box">
             <div class="widget-header">
                   <h4>Add Test Paper</h4>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                            <?php include('../class/esm.php'); ?>
                            <form method="post" action="" name="tt">
                                 <fieldset></fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Testpaper Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="name" id="form-field-1" placeholder="Test paper name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                 
                                       <fieldset></fieldset>
                                                
                                                 

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Medium Name </label>

                                                    <div class="col-sm-9">
                                                        <select name="medium" onchange="showclass(this.value)" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                            <option value="">Mediam</option>
                                                                <?php
                                                                $ins = $obj->selectAll('ams_madiam');
                                                                foreach ($ins as $roew):
                                                                    ?>

                                                                <option value="<?php echo $roew->id; ?>"><?php echo $roew->medium_name; ?></option>
                                                                 <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                       
                                       
                                       <fieldset></fieldset>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Class Name </label>

                                                    <div class="col-sm-9">
                                                        <select name="class" id="class"  class="col-xs-10 col-sm-10" >
                                                            <option value="">Select Class</option>
                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                 
                                        <fieldset></fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Details</label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="details" id="form-field-1" placeholder="Test paper name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                        
                                        <fieldset></fieldset>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Category</label>

                                                <div class="col-sm-9">
                                                    <label>
                                                        <input type="radio" onchange="showcategory(this.value)" name="category" value="1"/> 
                                                         Board
                                                    </label> 
                                                    
                                                    <label style="margin-left: 10px;">
                                                        <input type="radio" onchange="showcategory(this.value)" name="category" value="2"/> 
                                                         School
                                                    </label> 
                                                </div>
                                        </div>
                                        
                                        <fieldset></fieldset>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Board / School </label>

                                                    <div class="col-sm-9">
                                                        <select name="board" id="board"  class="col-xs-10 col-sm-10" >
                                                            <option value="">Select Class</option>
                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                        
                                        <fieldset></fieldset>
                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Year </label>

                                                    <div class="col-sm-9">
                                                        <select name="year" id="year"  class="col-xs-10 col-sm-10" >
                                                            <option value="">select year</option>
                                                            <option value="1">Half year</option>
                                                            <option value="2">anual</option>
                                                        </select>
                                                    </div>
                                                </div>
                                        
                                        <fieldset></fieldset>
                                          
                                        <div class="form-actions center">
                                            <button type="submit" name="submit" class="btn btn-sm btn-success">
                                                        Submit
                                                </button>
                                                
                                            <button type="reset" class="btn btn-sm btn-success">
                                                        Reset
                                                </button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
</div>
 <!----------------------widget end here-------------------------------------------------->  
 
 
 <!----------------------table start here-------------------------------------------------->   
<div class="col-xs-12 col-sm-6 widget-container-span">
<div class="widget-box">
 <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Test paper
            </h5>
 </div>

<div class="widget-body">
    <div class="widget-main no-padding">
    <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
          <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
            <thead>
                <tr>
                    
                <th class="center">
                    <label>

                            <span class="lbl">SL NO</span>
                    </label>
                </th>
                
                <th>Name</th>

                <th>
                        <i class="icon-time bigger-110 hidden-480"></i>
                        Date
                </th>
                
                <th class="center">Status</th>

                <th class="hidden-480">Edit</th>
                
                <th class="center">Delete</th>

                </tr>
            </thead>

            <tbody>
            <?php 
            $data=$obj->SelectAll($table);
            $x=1;
            if(!empty($data))
            foreach($data as $row):
            ?>
            <tr>
               <td class="center">
                <label>

                        <span class="lbl"><?php echo $x; ?></span>
                </label>
               </td>

            <td>
                 <?php echo $row->name; ?>
            </td>

            <td><?php echo $row->date; ?></td>

            <td class="hidden-480">
                
                <span class="label label-sm label-success">
                  successfully               
                </span>
                
            </td>
            
            <td class="center">
                
                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                        <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info" title="clik here to edit">
                            <i class="icon-edit bigger-110"></i>
                        </a>
                    <div id="modal-table<?php echo $row->id;?>" class="modal fade" tabindex="-1">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header no-padding">
                                    <div class="table-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                            <span class="white">&times;</span>
                                        </button>
                                        Edit Detail :
                                    </div>
                                </div>

                                <div class="modal-body no-padding">
                                    <div class="widget-body">
                                            <div class="widget-main no-padding">
                                               
                                                <form method="post" action="" name="tt">
                                                     <fieldset></fieldset>
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Testpaper Name </label>

                                                                    <div class="col-sm-9">
                                                                        <input name="id" type="hidden" value="<?php echo $row->id;?>"/>
                                                                        <input value="<?php echo $row->name;?>" type="text" name="paper" id="form-field-1" placeholder="Test paper name" class="col-xs-10 col-sm-10" />
                                                                    </div>
                                                            </div>


                                                            <fieldset></fieldset>

                                                            <div class="form-actions center">
                                                                <button type="submit" name="edit" class="btn btn-sm btn-success">
                                                                            Submit
                                                                    </button>

                                                                <button type="reset" name="reset" class="btn btn-sm btn-success">
                                                                            Reset
                                                                    </button>
                                                            </div>
                                                    </form>
                                            </div>
                                    </div>
                                </div>

                                <div class="modal-footer no-margin-top">
                                    <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                        <i class="icon-remove"></i>
                                        Close
                                    </button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- PAGE CONTENT ENDS -->

                </div>
                
            </td>

            
            <td class="center">
                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">  
                    <a href="<?php echo $obj->filename();?>?id=<?php echo $row->id;?>&AMP;action=delete" class="btn btn-xs btn-danger" title="clik here to delete">
                          <i class="icon-trash bigger-110"></i>
                     </a>
                </div>

             </td>
            </tr>
            <?php $x++; endforeach; ?>


        </tbody>

           </table>
        </div><!-- /.table-responsive -->
</div><!-- /span -->
</div><!-- /row -->
                </div>
  </div>
</div>
</div>
<!----------------------table end here-------------------------------------------------->   

<div class="col-xs-12">
    
</div><!-- /.col -->
</div>
</div>
</div>

<?php include("include_admin/left_select.php");?>

   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>
</div>

<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>



<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

		
	</body>


</html>
