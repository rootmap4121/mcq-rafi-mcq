 <div class="col-xs-12 col-sm-6 widget-container-span">
<div class="widget-box">
    <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Test paper
            </h5>
    </div>

        <div class="widget-body">
                <div class="widget-main no-padding">
                <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
                <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                                <tr>
                                        <th class="center">
                                                <label>
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl"></span>
                                                </label>
                                        </th>
                                        <th>Domain</th>
                                        <th>Price</th>
                                        <th class="hidden-480">Clicks</th>

                                        <th>
                                                <i class="icon-time bigger-110 hidden-480"></i>
                                                Update
                                        </th>
                                        <th class="hidden-480">Status</th>

                                        <th></th>
                                </tr>
                        </thead>

                        <tbody>

                                <tr>
                                        <td class="center">
                                                <label>
                                                        <input type="checkbox" class="ace" />
                                                        <span class="lbl"></span>
                                                </label>
                                        </td>

                                        <td>
                                                <a href="#">ace.com</a>
                                        </td>
                                        <td>$45</td>
                                        <td class="hidden-480">3,330</td>
                                        <td>Feb 12</td>

                                        <td class="hidden-480">
                                                <span class="label label-sm label-warning">Expiring</span>
                                        </td>

                                        <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                        <button class="btn btn-xs btn-success">
                                                                <i class="icon-ok bigger-120"></i>
                                                        </button>

                                                        <button class="btn btn-xs btn-info">
                                                                <i class="icon-edit bigger-120"></i>
                                                        </button>

                                                        <button class="btn btn-xs btn-danger">
                                                                <i class="icon-trash bigger-120"></i>
                                                        </button>

                                                        <button class="btn btn-xs btn-warning">
                                                                <i class="icon-flag bigger-120"></i>
                                                        </button>
                                                </div>

                                                <div class="visible-xs visible-sm hidden-md hidden-lg">
                                                        <div class="inline position-relative">
                                                                <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
                                                                        <i class="icon-cog icon-only bigger-110"></i>
                                                                </button>

                                                                <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                        <li>
                                                                                <a href="#" class="tooltip-info" data-rel="tooltip" title="View">
                                                                                        <span class="blue">
                                                                                                <i class="icon-zoom-in bigger-120"></i>
                                                                                        </span>
                                                                                </a>
                                                                        </li>

                                                                        <li>
                                                                                <a href="#" class="tooltip-success" data-rel="tooltip" title="Edit">
                                                                                        <span class="green">
                                                                                                <i class="icon-edit bigger-120"></i>
                                                                                        </span>
                                                                                </a>
                                                                        </li>

                                                                        <li>
                                                                                <a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
                                                                                        <span class="red">
                                                                                                <i class="icon-trash bigger-120"></i>
                                                                                        </span>
                                                                                </a>
                                                                        </li>
                                                                </ul>
                                                        </div>
                                                </div>
                                        </td>
                                </tr>


                </tbody>
                       
                   </table>
                </div><!-- /.table-responsive -->
        </div><!-- /span -->
</div><!-- /row -->
                </div>
        </div>
</div>
</div>