<div class="sidebar" id="sidebar">
    <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
            <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
                    <button class="btn btn-success">
                            <i class="icon-signal"></i>
                    </button>

                    <button class="btn btn-info">
                            <i class="icon-pencil"></i>
                    </button>

                    <button class="btn btn-warning">
                            <i class="icon-group"></i>
                    </button>

                    <button class="btn btn-danger">
                            <i class="icon-cogs"></i>
                    </button>
            </div>

            <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span>

                    <span class="btn btn-info"></span>

                    <span class="btn btn-warning"></span>

                    <span class="btn btn-danger"></span>
            </div>
    </div><!-- #sidebar-shortcuts -->

    <ul class="nav nav-list">
            <li>
                    <a href="">
                            <i class="icon-dashboard"></i>
                            <span class="menu-text"> Dashboard </span>
                    </a>
            </li>


            <li>
                    <a href="#" class="dropdown-toggle">
                            
                            <span class="menu-text">MCQ</span>

                            <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                            <li>
                                <a href="student.php">
                                            <i class="icon-double-angle-right"></i>
                                           Add Student
                                    </a>
                            </li>

                            <li>
                                    <a href="class.php">
                                            <i class="icon-double-angle-right"></i>
                                          Add Class
                                    </a>
                            </li>

                            <li>
                                    <a href="teacher.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Teacher
                                    </a>
                            </li>

                            <li>
                                    <a href="institute.php">
                                            <i class="icon-double-angle-right"></i>
                                           Add Institute
                                    </a>
                            </li>

                            <li>
                                    <a href="medium.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Medium
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="notic.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Notice
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="answer.php">
                                            <i class="icon-double-angle-right"></i>
                                           Add Answer
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="exam.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Exam
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="test_paper.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Test Paper
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="subject.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Subject
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="note.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Notes
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="summerry.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Summerry
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="susetion.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Susetion
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="question.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Question
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="answer_selection.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Answer Selection
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="board_school.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Board And school
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="category.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add Category
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="mcq_data.php">
                                            <i class="icon-double-angle-right"></i>
                                            Add MCQ DATA
                                    </a>
                            </li>
                            
                            

                            <li>
                                <a href="#" class="dropdown-toggle">
                                        <i class="icon-double-angle-right"></i>

                                        Three Level Menu
                                        <b class="arrow icon-angle-down"></b>
                                </a>

                                    <ul class="submenu">
                                            <li>
                                                    <a href="#">
                                                            <i class="icon-leaf"></i>
                                                            Item #1
                                                    </a>
                                            </li>

                                            <li>
                                                    <a href="#" class="dropdown-toggle">
                                                            <i class="icon-pencil"></i>

                                                            4th level
                                                            <b class="arrow icon-angle-down"></b>
                                                    </a>

                                                    <ul class="submenu">
                                                            <li>
                                                                    <a href="#">
                                                                            <i class="icon-plus"></i>
                                                                            Add Product
                                                                    </a>
                                                            </li>

                                                            <li>
                                                                    <a href="#">
                                                                            <i class="icon-eye-open"></i>
                                                                            View Products
                                                                    </a>
                                                            </li>
                                                    </ul>
                                            </li>
                                    </ul>
                            </li>
                    </ul>
            </li>
             <li>
                    <a href="mcq_multi_answer.php"> 
                        <span class="menu-text"> Multipel Answer </span>
                    </a>
            </li>
            
            <li>
                    <a href="mcq_select_answer.php"> 
                        <span class="menu-text"> Multipel Select Answer </span>
                    </a>
            </li>
            <li>
                    <a href="#" class="dropdown-toggle">
                            
                            <span class="menu-text">Data Tables </span>

                            <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Student
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Subject
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Class
                                    </a>
                            </li>
                            
                             <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Exam
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Medium
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Question
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Answer
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Institute
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Teacher
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Summery
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Suetion
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Test-Paper
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Note
                                    </a>
                            </li>
                            
                            <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Notic
                                    </a>
                            </li>
                            
                             <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Board and School
                                    </a>
                            </li>
                            
                             <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Category
                                    </a>
                            </li>
                            
                             <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            Answer Selection
                                    </a>
                            </li>
                            
                             <li>
                                    <a href="">
                                            <i class="icon-double-angle-right"></i>
                                            MCQ Data
                                    </a>
                            </li>

                    </ul>
            </li>

            <li>
                    <a href="#" class="dropdown-toggle">
                            
                            <span class="menu-text"> Forms </span>

                            <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                            <li>
                                    <a href="form-elements.html">
                                            <i class="icon-double-angle-right"></i>
                                            Form Elements
                                    </a>
                            </li>

                            <li>
                                    <a href="form-wizard.html">
                                            <i class="icon-double-angle-right"></i>
                                            Wizard &amp; Validation
                                    </a>
                            </li>

                            <li>
                                    <a href="wysiwyg.html">
                                            <i class="icon-double-angle-right"></i>
                                            Wysiwyg &amp; Markdown
                                    </a>
                            </li>

                            <li>
                                    <a href="dropzone.html">
                                            <i class="icon-double-angle-right"></i>
                                            Dropzone File Upload
                                    </a>
                            </li>
                    </ul>
            </li>

            <li>
                    <a href="widgets.html">
                           
                            <span class="menu-text"> Widgets </span>
                    </a>
            </li>

            <li>
                    <a href="calendar.html">
                            <i class="icon-calendar"></i>

                            <span class="menu-text">
                                    Calendar
                                    <span class="badge badge-transparent tooltip-error" title="2&nbsp;Important&nbsp;Events">
                                            <i class="icon-warning-sign red bigger-130"></i>
                                    </span>
                            </span>
                    </a>
            </li>

            <li>
                    <a href="mcq_gallery.php">
                            
                            <span class="menu-text"> Gallery </span>
                    </a>
            </li>
            
            <li>
                    <a href="img_title.php">
                            
                            <span class="menu-text"> Image Title </span>
                    </a>
            </li>
            
            <li>
                    <a href="add_list.php">
                            
                            <span class="menu-text"> Add List </span>
                    </a>
            </li>

            <li>
                    <a href="#" class="dropdown-toggle">
                           
                            <span class="menu-text"> More Pages </span>

                            <b class="arrow icon-angle-down"></b>
                    </a>

                    <ul class="submenu">
                            <li>
                                    <a href="profile.html">
                                            <i class="icon-double-angle-right"></i>
                                            User Profile
                                    </a>
                            </li>

                            <li>
                                    <a href="inbox.html">
                                            <i class="icon-double-angle-right"></i>
                                            Inbox
                                    </a>
                            </li>

                            <li>
                                    <a href="pricing.html">
                                            <i class="icon-double-angle-right"></i>
                                            Pricing Tables
                                    </a>
                            </li>

                            <li>
                                    <a href="invoice.html">
                                            <i class="icon-double-angle-right"></i>
                                            Invoice
                                    </a>
                            </li>

                            <li>
                                    <a href="timeline.html">
                                            <i class="icon-double-angle-right"></i>
                                            Timeline
                                    </a>
                            </li>

                            <li>
                                    <a href="login.html">
                                            <i class="icon-double-angle-right"></i>
                                            Login &amp; Register
                                    </a>
                            </li>
                    </ul>
            </li>

            
    </ul><!-- /.nav-list -->

    <div class="sidebar-collapse" id="sidebar-collapse">
            <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>
    </div>

    <script type="text/javascript">
            try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
    </script>
</div>