<?php
include('../class/auth.php');
$table = 'ams_multipel_answer';
if (isset($_POST['submit'])) {
    $exist = array("question_id" => $_POST['question'],"medium_id" => $_POST['medium'],"subject_id" => $_POST['subject'], "name" => $_POST['answer'],"status" => $_POST['category'],"category_id"=>$_POST['category'],"class_id"=>$_POST['class']);
    $insert = array("question_id" => $_POST['question'],"category_id"=>$_POST['category'],"class_id"=>$_POST['class'],"medium_id" => $_POST['medium'],"subject_id" => $_POST['subject'], "name" => $_POST['answer'], "date" => date('y-m-d'), "status" => $_POST['category']);

    if ($obj->exists_Multiple($table, $exist) == 1) {
        $errmsg_arr[] = 'Already Exists';
        $error_flag = true;

        if ($error_flag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header('location:' . $obj->filename());
            exit();
        }
    } else {
        if ($obj->insert($table, $insert) == 1) {
            $errmsg_arr[] = 'Successfully Saved';
            $error_flag = true;
            if ($error_flag) {
                $_SESSION['SMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header('location:' . $obj->filename());
                exit();
            }
        } else {
            $errmsg_arr[] = 'Failed to saved';
            $error_flag = true;
            if ($error_flag) {
                $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
                session_write_close();
                header('location:' . $obj->filename());
                exit();
            }
        }
    }
}

if (isset($_POST['edit'])) {
    $update = array("id" => $_POST['id'],"question_id" => $_POST['ques'],"medium_id" => $_POST['medium'],"subject_id" => $_POST['subject_mcq'], "name" => $_POST['answer'],"status" => $_POST['category']);
    if ($obj->update($table, $update) == 1) {
        $errmsg_arr[] = 'Successfully Updated id :' . $_POST['id'];
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header('location:' . $obj->filename());
            exit();
        }
    } else {
        $errmsg_arr[] = 'Failed to Update';
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header('location:' . $obj->filename());
            exit();
        }
    }
}

if (@$_GET['action'] == 'delete') {
    $delete = array("id" => $_GET['id']);
    if ($obj->delete($table, $delete) == 1) {
        $errmsg_arr[] = 'Successfully Deleted From List (id):' . $_GET['id'];
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['SMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header('location:' . $obj->filename());
            exit();
        }
    } else {
        $errmsg_arr[] = 'Failed to saved';
        $error_flag = true;
        if ($error_flag) {
            $_SESSION['ERRMSG_ARR'] = $errmsg_arr;
            session_write_close();
            header('location:' . $obj->filename());
            exit();
        }
    }
}
?>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Multipel - MCQ Admin</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
        <link rel="stylesheet" href="assets/css/ace.min.css" />
        <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
        <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
        <script src="assets/js/ace-extra.min.js"></script>
        
         <script>
            function showcat(str)
            {
                if (str == "")
                {
                    document.getElementById("category").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("category").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/category.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        
        <script>
            function showclas(str)
            {
                if (str == "")
                {
                    document.getElementById("class").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("class").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/class.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        
        <script>
            function showsubj(str)
            {
                if (str == "")
                {
                    document.getElementById("subject").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("subject").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/class_sub.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        
        <script>
            function showquestion(str)
            {
                if (str == "")
                {
                    document.getElementById("question").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("question").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/question.php?q="+str, true);
                xmlhttp.send();
            }
        </script>

    </head>

    <body>
<?php include("include_admin/head.php"); ?>

        <div class="main-container" id="main-container">
            <script type="text/javascript">
                try {
                    ace.settings.check('main-container', 'fixed')
                } catch (e) {
                }
            </script>

            <div class="main-container-inner">
                <a class="menu-toggler" id="menu-toggler" href="#">
                    <span class="menu-text"></span>
                </a>

<?php include("include_admin/side_manu.php"); ?>

                <div class="main-content">

<?php include("include_admin/other_home.php"); ?>

                    <div class="page-content">
                        <div class="row">
                            <!----------------------widget start here--------------------------------------------------> 
                            <div class="col-sm-6">
                                <div class="widget-box">
                                    <div class="widget-header">
                                        <h4>Add Multipel Answer</h4>
                                    </div>

                                    <div class="widget-body">
                                        <div class="widget-main no-padding">
                                          <?php include ('../class/esm.php'); ?>
                                            <form method="post" action="" name="madium">

                                                 
                                                  
                                                  <fieldset></fieldset>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Medium name </label>

                                                    <div class="col-sm-9">
                                                        <select name="medium" onchange="showcat(this.value)" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                            <option value="">Medium Name</option>
                                                                <?php
                                                                $ins = $obj->selectAll('ams_madiam');
                                                                foreach ($ins as $row):
                                                                    ?>

                                                                <option value="<?php echo $row->id; ?>"><?php echo $row->medium_name; ?></option>
                                                                 <?php endforeach; ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                  
                                                  
                                                  <fieldset></fieldset>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Category name </label>

                                                    <div class="col-sm-9">
                                                        <select name="category" id="category" onchange="showclas(this.value)" class="col-xs-10 col-sm-10" >
                                                            <option value="">Category Name</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                  
                                                    <fieldset></fieldset>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Class name </label>

                                                    <div class="col-sm-9">
                                                        <select name="class" id="class" onchange="showsubj(this.value)" id="subject_mcq" class="col-xs-10 col-sm-10" >
                                                            <option value="">Class Name</option>
                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                                  
                                                  <fieldset></fieldset>
                                                  
                                                  


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Subject name </label>

                                                    <div class="col-sm-9">
                                                        <select name="subject" id="subject" onchange="showquestion(this.value)" id="subject_mcq" class="col-xs-10 col-sm-10" >
                                                            <option value="">Subject Name</option>
                                                                
                                                        </select>
                                                    </div>
                                                </div>
                                                  
                                                  
                                                  <fieldset></fieldset>


                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Question name </label>

                                                    <div class="col-sm-9">
                                                        <select name="question" id="question" class="col-xs-10 col-sm-10" >
                                                            <option value="">Question Name</option>
                                                               
                                                        </select>
                                                    </div>
                                                </div>
                                                  
                                                <fieldset></fieldset>

                                                <div class="form-group">
                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Multipel Answer</label>

                                                    <div class="col-sm-9">
                                                        <input type="text" name="answer" id="form-field-1" placeholder="Test multipel answer" class="col-xs-10 col-sm-10" />
                                                    </div>
                                                </div>
                                                
                                               
                                                
                                                <fieldset></fieldset>

                                                <div class="form-actions center">
                                                    <button type="submit" name="submit" class="btn btn-sm btn-success">
                                                        Submit
                                                    </button>

                                                    <button type="reset" class="btn btn-sm btn-success">
                                                        Reset
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!----------------------widget end here-------------------------------------------------->   


        <div class="col-xs-12 col-sm-6 widget-container-span">
            <div class="widget-box">
                <div class="widget-header header-color-blue">
                    <h5 class="bigger lighter">
                        <i class="icon-table"></i>
                        List of  Multipel Answer
                    </h5>
                </div>

                <div class="widget-body">
                    <div class="widget-main no-padding">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="table-responsive">
                                    <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                                        <thead>
                                            <tr>
                                                <th class="center">
                                                    SL no   
                                                </th>
                                                <th>Answer</th>

                                                <th>Medium</th>
                                                <th>Subject</th>

                                                <th>Question</th>





                                                <th>
                                                    Edit
                                                </th>
                                                <th class="hidden-480">Delet</th>

                                            </tr>
                                        </thead>

                                            <tbody>
<?php
                                        $question = $obj->selectAll($table);
                                        $s = 1;
                                        if(!empty($question))
                                        foreach ($question as $row):
                                            ?>
                                                <tr>
                                                    <td class="center">
                                                        <label>
                                                            <span class="lbl"><?php echo $s; ?></span>
                                                        </label>
                                                    </td>

                                                    <td>
                                                        <?php echo $row->name; ?>
                                                    </td>

                                                    <td>
                                                        <?php echo $obj->SelectAllByVal("ams_madiam","id",$row->medium_id,"medium_name");?>
                                                    </td>

                                                    <td class="hidden-480">
                                                        <?php echo $obj->SelectAllByVal("ams_subject","id",$row->subject_id,"sub_name");?>
                                                    </td>

                                                    <td >
                                                        <?php echo $obj->SelectAllByVal("ams_question","id",$row->question_id,"ques_name");?>
                                                    </td>

                                                    <td class="hidden-480">

                                                        <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                            <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info">
                                                                <i class="icon-edit bigger-120"></i>
                                                            </a>
                                                            <div id="modal-table<?php echo $row->id; ?>" class="modal fade" tabindex="-1">
                                                                <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header no-padding">
                                                                            <div class="table-header">
                                                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                                    <span class="white">&times;</span>
                                                                                </button>
                                                                                Edit Detail :
                                                                            </div>
                                                                        </div>

                                                                        <div class="modal-body no-padding">
                                                                            <!--FORM FOR edit start-->
                                                                            <div class="widget-body">
                                                                        <div class="widget-main no-padding">
                                                                         
                                                                            <form method="post" action="" name="edit">



                                                                                  <fieldset></fieldset>
                                                                                  

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Medium name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <input type="hidden" name="id" value="<?php echo $row->id;?>"/>
                                                                                        <select name="medium" id="form-field-1" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Medium Name</option>
                                                                                                <?php
                                                                                                $ins = $obj->selectAll('ams_madiam');
                                                                                                foreach ($ins as $rxow):
                                                                                                    ?>

                                                                                                <option value="<?php echo $rxow->id; ?>"><?php echo $rxow->medium_name; ?></option>
                                                                                                 <?php endforeach; ?>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>



                                                                                  <fieldset></fieldset>


                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Subject name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="subject_mcq" onchange="showquesti(this.value)" id="subject_mcq" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Subject Name</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                  <fieldset></fieldset>


                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Question name </label>

                                                                                    <div class="col-sm-9">
                                                                                        <select name="ques" id="ques" class="col-xs-10 col-sm-10" >
                                                                                            <option value="">Question Name</option>

                                                                                        </select>
                                                                                    </div>
                                                                                </div>

                                                                                <fieldset></fieldset>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Multipel Answer</label>

                                                                                    <div class="col-sm-9">
                                                                                        
                                                                                        <input type="text" name="answer" value="<?php echo $row->name; ?>" id="form-field-1" placeholder="Test Question name" class="col-xs-10 col-sm-10" />
                                                                                     
                                                                                    </div>
                                                                                </div>




                                                                                <fieldset></fieldset>

                                                                                <div class="form-actions center">
                                                                                    <button type="submit" name="edit" class="btn btn-sm btn-success">
                                                                                        Submit
                                                                                    </button>

                                                                                    <button type="reset" class="btn btn-sm btn-success">
                                                                                        Reset
                                                                                    </button>
                                                                                </div>
                                                                              
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                    <!--form for edit end-->
                                                                        </div>

                                                                        <div class="modal-footer no-margin-top">
                                                                            <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                                                                <i class="icon-remove"></i>
                                                                                Close
                                                                            </button>
                                                                        </div>
                                                                    </div><!-- /.modal-content -->
                                                                </div><!-- /.modal-dialog -->
                                                            </div><!-- PAGE CONTENT ENDS -->
                                                        </div>

                                                    </td>

                                                    <td>
                                                        <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                                                            <a href="<?php echo $obj->filename(); ?>?id=<?php echo $row->id; ?>&AMP;action=delete" class="btn btn-xs btn-danger">
                                                                <i class="icon-trash bigger-120"></i>                                                                                </a>
                                                        </div>


                                                    </td>
                                                </tr>

                                                <?php $s++;
                                            endforeach;
                                            ?> 
                                            </tbody>

                                    </table>
                                </div><!-- /.table-responsive -->
                            </div><!-- /span -->
                        </div><!-- /row -->
                    </div>
                </div>
            </div>
        </div>




        <div class="col-xs-12">

        </div><!-- /.col -->
    </div>
</div>
</div>

<?php include("include_admin/left_select.php"); ?>

                <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
                    <i class="icon-double-angle-up icon-only bigger-110"></i>
                </a>
            </div>

            <script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



            <script type="text/javascript">
                window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>" + "<" + "/script>");
            </script>



            <script type="text/javascript">
                if ("ontouchend" in document)
                    document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");</script>
            <script src="assets/js/bootstrap.min.js"></script>
            <script src="assets/js/typeahead-bs2.min.js"></script>



            <script src="assets/js/ace-elements.min.js"></script>
            <script src="assets/js/ace.min.js"></script>


    </body>


</html>
