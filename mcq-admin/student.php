<?php
include_once("../class/auth.php");
$table='ams_student';
$w=250; $h=250; $thumb="../photo/";
if(isset($_POST['submit']))
{
     if(!empty($_POST['fname']) && !empty($_POST['lname']))
        {
        $exist=array("name"=>$_POST['name']);
        if(!empty($_FILES['image']['name']))
        {
        $files =$obj->image_bigcaption($w,$h,$thumb);
        $photo=substr($files,9,1600);
        }
        else 
        {
            $photo="";
        }
    $exist=array("email"=>$_POST['email']);
    $insert=array("fname"=>$_POST['fname'],"lname"=>$_POST['lname'],"m_name"=>$_POST['mother'],
    "f_name"=>$_POST['father'],"email"=>$_POST['email'],"gender"=>$_POST['gender'],"dob"=>$_POST['dob'],
    "region"=>$_POST['region'],"s_mobile"=>$_POST['s_number'],"f_mubile"=>$_POST['f_number'],"inst_id"=>$_POST['institut'],
    "inst_id"=>$_POST['institut'],"national"=>$_POST['nationality'],"photo"=>$photo,
    "addrs"=>$_POST['addreces'],"user_name"=>$_POST['user'],"pass"=>$_POST['pass'],
    "date"=> date('y-m-d'),"status"=>1);
    
    if($obj->exists($table,$exist)==1)
    {
        $errmsg_arr[]='Already Exists';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $errmsg_arr[]='successfully saved';
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$errmsg_arr;
             session_write_close();
             header('location:'.$obj->filename());
             exit();
         }
    }
 else
     {
      $errmsg_arr[]='failed to saved';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
     }
    }
}
if(isset($_POST['edit']))
{
    $edit=array("id"=>$_POST['id'],"fname"=>$_POST['fname'],"lname"=>$_POST['lname'],"m_name"=>$_POST['mother'],
    "f_name"=>$_POST['father'],"email"=>$_POST['email'],"gender"=>$_POST['gender'],"dob"=>$_POST['dob'],
    "region"=>$_POST['region'],"s_mobile"=>$_POST['s_number'],"f_mubile"=>$_POST['f_number'],"inst_id"=>$_POST['institut'],
    "inst_id"=>$_POST['institut'],"national"=>$_POST['nationality'],
    "addrs"=>$_POST['addreces'],"user_name"=>$_POST['user'],"pass"=>$_POST['pass'],
    "date"=> date('y-m-d'),"status"=>1);
    if($obj->update($table,$edit)==1)
    {
        $errmsg_arr[]='Successfully update'.$_POST['id'];
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
      $errmsg_arr[]='Update Failed';
      $error_flag=true;
      if($error_flag)
      {
          $_SESSION['ERRMSG_ARR']=$errmsg_arr;
          session_write_close();
          header('location:'.$obj->filename());
          exit();
      }
    }
}
if(@$_GET['action']== 'delete')
{
    $delet=array("id"=>$_GET['id']);
    if($obj->delete($table,$delet)==1)
    {
        $errmsg_arr[]='Successfully Deleted';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['SMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }
    }
 else
    {
     $errmsg_arr[]='Delete failed';
        $error_flag=true;
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$errmsg_arr;
            session_write_close();
            header('location:'.$obj->filename());
            exit();
        }   
    }
}}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Student - MCQ Admin</title>
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/css/font-awesome.min.css" />
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400,300" />
    <link rel="stylesheet" href="assets/css/ace.min.css" />
    <link rel="stylesheet" href="assets/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="assets/css/ace-skins.min.css" />
    <script src="assets/js/ace-extra.min.js"></script>

</head>

<body>
<?php include("include_admin/head.php");?>

<div class="main-container" id="main-container">
    <script type="text/javascript">
        try{ace.settings.check('main-container' , 'fixed')}catch(e){}
    </script>

 <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
                <span class="menu-text"></span>
        </a>

                <?php include("include_admin/side_manu.php");?>

                <div class="main-content">
					
                <?php include("include_admin/other_home.php");?>

<div class="page-content">
        <div class="row">
<!----------------------widget start here--------------------------------------------------> 
<div class="col-sm-6">
        <div class="widget-box">
             <div class="widget-header">
                   <h4>Add Student</h4>
                </div>

                <div class="widget-body">
                        <div class="widget-main no-padding">
                            <?php include ('../class/esm.php');?>
                            <form method="post" action="" name="stu" enctype="multipart/form-data">
                                       
                                        
                                        
                                       <fieldset></fieldset>
                                       
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Student Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="fname" id="form-field-1" placeholder="type your fast name" class="col-xs-10 col-sm-5" />
                                                    <input type="text" name="lname" id="form-field-1" placeholder="type your last name" class="col-xs-10 col-sm-5" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Father's Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="father" id="form-field-1" placeholder="Type user mother name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Birthday</label>
                                           <div class="col-xs-8 col-sm-7">
                                                <div class="input-group">
                                                    <input name="dob" class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                        <span class="input-group-addon">
                                                                <i class="icon-calendar bigger-110"></i>
                                                        </span>
                                                </div>
                                        </div>

                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Mother's Name </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="mother" id="form-field-1" placeholder="Type user mother name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">E-mail  </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="email" id="form-field-1" placeholder="Type user E-mail" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">User Name  </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="user" id="form-field-1" placeholder="Type user name" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Student Number </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="s_number" id="form-field-1" placeholder="type your number" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">father's Number  </label>

                                                <div class="col-sm-9">
                                                    <input type="text" name="f_number" id="form-field-1" placeholder="type your father's number" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Gender  </label>

                                                <div class="col-sm-9">
                                                    <label> <input value="male" type="radio" name="gender" id="form-field-1" />  Male</label>
                                                    <label><input type="radio" value="female" name="gender" id="form-field-1" />Female</label>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Region</label>

                                                <div class="col-sm-9">
                                                    <select name="region" class="col-xs-10 col-sm-10">
                                                        <option>Region</option>
                                                        <option>bangladesh</option>
                                                    </select>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Instituation's Name </label>

                                                <div class="col-sm-9">
                                                    <select name="institut" class="col-xs-10 col-sm-10">
                                                    <option>Institute name</option>
                                                     <?php 
                                                     $insti=$obj->selectAll('ams_instet');
                                                     foreach ($insti as $row):
                                                    ?>
                                                    <option value="<?php echo $row->inst_name;?>"><?php echo $row->inst_name;?></option>
                                                     <?php endforeach;?>
                                                    </select>
                                                   
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nationality  </label>

                                                <div class="col-sm-9">
                                                    <select name="nationality" class="col-xs-10 col-sm-10">
                                                        <option>Nationality</option>
                                                    </select>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Addreces  </label>

                                                <div class="col-sm-9">
                                                    <textarea name="addreces" class="col-xs-10 col-sm-10" placeholder="type your addreces"></textarea>
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Password </label>

                                                <div class="col-sm-9">
                                                    <input type="password" name="pass" id="form-field-1" placeholder="type your password" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                       
                                       <div class="form-group">
                                            <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image </label>

                                                <div class="col-sm-9">
                                                    <input type="file" name="image" id="form-field-1" class="col-xs-10 col-sm-10" />
                                                </div>
                                        </div>
                                       <fieldset></fieldset>
                                          
                                        <div class="form-actions center">
                                            <button type="submit" name="submit" class="btn btn-sm btn-success">
                                                        Submit
                                                </button>
                                                
                                            <button type="reset" class="btn btn-sm btn-success">
                                                        Reset
                                                </button>
                                        </div>
                                </form>
                        </div>
                </div>
        </div>
</div>
 <!----------------------widget end here-------------------------------------------------->   
 

  <div class="col-xs-12 col-sm-6 widget-container-span">
<div class="widget-box">
    <div class="widget-header header-color-blue">
            <h5 class="bigger lighter">
                    <i class="icon-table"></i>
                    List of Teacher
            </h5>
    </div>

        <div class="widget-body">
                <div class="widget-main no-padding">
                <div class="row">
<div class="col-xs-12">
        <div class="table-responsive">
            <table aria-describedby="sample-table-2_info" id="sample-table-2" class="table table-striped table-bordered table-hover dataTable">
                        <thead>
                                <tr>
                                        <th class="">
                                          SL no
                                        </th>
                                        
                                        <th>Name</th>
                                        
                                        <th>Father's name</th>
                                        
                                        <th>Mother's name</th>
                                        
                                       
                                        <th>
                                         <i class="icon-time bigger-110 hidden-480"></i>
                                          Date
                                        </th>
                                        
                                        <th >Status</th>
                                        
                                        <th >Edit</th>

                                        <th>delet</th>
                                </tr>
                        </thead>
               <?php 
               $student=$obj->selectAll($table);
               $s=1;
               if(!empty($student))
               foreach ($student as $row):
               ?>
                        <tbody>

                                <tr>
                                        <td class="center">
                                            <label>
                                               <span class="lbl"><?php echo $s;?></span>
                                            </label>
                                        </td>

                                        <td>
                                                <a href="#"><?php echo $row->fname;?> <?php echo $row->lname;?></a>
                                        </td>
                                        
                                        <td><?php echo $row->f_name;?></td>
                                        
                                        <td><?php echo $row->m_name;?></td>
                                        
                                       
                                        <td><img src="../photo/<?php echo $row->photo;?>" width="50" height="50" /></td>

                                        <td class="hidden-480">
                                                <span class="label label-sm label-warning"><?php echo $row->status;?></span>
                                        </td>
                                        
                                        <td class="hidden-480">
                                         <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">   
                                        <a href="#modal-table<?php echo $row->id; ?>" role="button" data-toggle="modal" class="btn btn-xs btn-info">
                                                <i class="icon-edit bigger-120"></i>
                                        </a>
                                             <div id="modal-table<?php echo $row->id;?>" class="modal fade" tabindex="-1">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header no-padding">
                                                                <div class="table-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        <span class="white">&times;</span>
                                                                    </button>
                                                                    Edit Detail :
                                                                </div>
                                                            </div>

                                                            <div class="modal-body no-padding">
                                                               <div class="widget-body">
                                                                <div class="widget-main no-padding">
                                                                    
                                                                    <form method="post" action="" name="stu">



                                                                               <fieldset></fieldset>

                                                                                <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Student Name </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="fname" id="form-field-1" placeholder="type your fast name" class="col-xs-10 col-sm-5" />
                                                                                            <input type="text" name="lname" id="form-field-1" placeholder="type your last name" class="col-xs-10 col-sm-5" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>


                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Father's Name </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="father" id="form-field-1" placeholder="Type user mother name" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Birthday</label>
                                                                                   <div class="col-xs-8 col-sm-7">
                                                                                        <div class="input-group">
                                                                                            <input name="dob" class="form-control date-picker" id="id-date-picker-1" type="text" data-date-format="yyyy-mm-dd" />
                                                                                                <span class="input-group-addon">
                                                                                                        <i class="icon-calendar bigger-110"></i>
                                                                                                </span>
                                                                                        </div>
                                                                                </div>

                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Mother's Name </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="mother" id="form-field-1" placeholder="Type user mother name" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">E-mail  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="email" id="form-field-1" placeholder="Type user E-mail" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">User Name  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="user" id="form-field-1" placeholder="Type user name" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Student Number </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="s_number" id="form-field-1" placeholder="type your number" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">father's Number  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="text" name="f_number" id="form-field-1" placeholder="type your father's number" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Gender  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <label> <input value="male" type="radio" name="gender" id="form-field-1" />  Male</label>
                                                                                            <label><input type="radio" value="female" name="gender" id="form-field-1" />Female</label>
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Region</label>

                                                                                        <div class="col-sm-9">
                                                                                            <select name="region" class="col-xs-10 col-sm-10">
                                                                                                <option>Region</option>
                                                                                                <option>bangladesh</option>
                                                                                            </select>
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Instituation's Name </label>

                                                                                        <div class="col-sm-9">
                                                                                            <select name="institut" class="col-xs-10 col-sm-10">
                                                                                            <option>Institute name</option>
                                                                                             <?php 
                                                                                             $insti=$obj->selectAll('ams_instet');
                                                                                             foreach ($insti as $rrow):
                                                                                            ?>
                                                                                            <option value="<?php echo $rrow->inst_name;?>"><?php echo $rrow->inst_name;?></option>
                                                                                             <?php endforeach;?>
                                                                                            </select>

                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Nationality  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <select name="nationality" class="col-xs-10 col-sm-10">
                                                                                                <option>Nationality</option>
                                                                                            </select>
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Addreces  </label>

                                                                                        <div class="col-sm-9">
                                                                                            <textarea name="addreces" class="col-xs-10 col-sm-10" placeholder="type your addreces"></textarea>
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Password </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="password" name="pass" id="form-field-1" placeholder="type your password" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                               <div class="form-group">
                                                                                    <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Image </label>

                                                                                        <div class="col-sm-9">
                                                                                            <input type="file" name="img" id="form-field-1" class="col-xs-10 col-sm-10" />
                                                                                        </div>
                                                                                </div>
                                                                               <fieldset></fieldset>

                                                                                <div class="form-actions center">
                                                                                    <button type="submit" name="edit" class="btn btn-sm btn-success">
                                                                                                Submit
                                                                                        </button>

                                                                                    <button type="reset" name="reset" class="btn btn-sm btn-success">
                                                                                                Reset
                                                                                        </button>
                                                                                </div>
                                                                        </form>
                                                                </div>
                                                        </div>
                                                                                                    </div>

                                                            <div class="modal-footer no-margin-top">
                                                                <button class="btn btn-sm btn-danger pull-left" data-dismiss="modal">
                                                                    <i class="icon-remove"></i>
                                                                    Close
                                                                </button>
                                                            </div>
                                                        </div><!-- /.modal-content -->
                                                    </div><!-- /.modal-dialog -->
                                                </div><!-- PAGE CONTENT ENDS -->

                                             
                                         </div>
                                        </td>

                                        <td>
                                                <div class="visible-md visible-lg hidden-sm hidden-xs btn-group"> 
                                                    <a href="<?php echo $obj->filename();?>?id=<?php echo $row->id;?>&AMP;action=delete" class="btn btn-xs btn-danger">
                                                                <i class="icon-trash bigger-120"></i>
                                                        </a>

                                                </div>
                                        </td>
                                </tr>


                </tbody>
                      <?php $s++; endforeach;?> 
                   </table>
                </div><!-- /.table-responsive -->
        </div><!-- /span -->
</div><!-- /row -->
                </div>
        </div>
</div>
</div>
 
 


<div class="col-xs-12">
    
</div><!-- /.col -->
</div>
</div>
</div>

<?php include("include_admin/left_select.php");?>

   <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
       <i class="icon-double-angle-up icon-only bigger-110"></i>
   </a>
</div>

<script src="../../../../ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>



<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
</script>



<script type="text/javascript">
        if("ontouchend" in document) document.write("<script src='assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");</script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/typeahead-bs2.min.js"></script>
<script src="assets/js/jquery-ui-1.10.3.custom.min.js"></script>
		<script src="assets/js/jquery.ui.touch-punch.min.js"></script>
		<script src="assets/js/chosen.jquery.min.js"></script>
		<script src="assets/js/fuelux/fuelux.spinner.min.js"></script>
		<script src="assets/js/date-time/bootstrap-datepicker.min.js"></script>
		<script src="assets/js/date-time/bootstrap-timepicker.min.js"></script>
		<script src="assets/js/date-time/moment.min.js"></script>
		<script src="assets/js/date-time/daterangepicker.min.js"></script>
		<script src="assets/js/bootstrap-colorpicker.min.js"></script>
		<script src="assets/js/jquery.knob.min.js"></script>
		<script src="assets/js/jquery.autosize.min.js"></script>
		<script src="assets/js/jquery.inputlimiter.1.3.1.min.js"></script>
		<script src="assets/js/jquery.maskedinput.min.js"></script>
		<script src="assets/js/bootstrap-tag.min.js"></script>
		

<script>
jQuery(function($) {
							
				$.mask.definitions['~']='[+-]';
				$('.input-mask-date').mask('99/99/9999');
				
			
			
				
				$('#id-input-file-1 , #id-input-file-2').ace_file_input({
					no_file:'No File ...',
					btn_choose:'Choose',
					btn_change:'Change',
					droppable:false,
					onchange:null,
					thumbnail:false //| true | large
					//whitelist:'gif|png|jpg|jpeg'
					//blacklist:'exe|php'
					//onchange:''
					//
				});
				
				$('#id-input-file-3').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'icon-cloud-upload',
					droppable:true,
					thumbnail:'small'//large | fit
					//,icon_remove:null//set null, to hide remove/reset button
					/**,before_change:function(files, dropped) {
						//Check an example below
						//or examples/file-upload.html
						return true;
					}*/
					/**,before_remove : function() {
						return true;
					}*/
					,
					preview_error : function(filename, error_code) {
						//name of the file that failed
						//error_code values
						//1 = 'FILE_LOAD_FAILED',
						//2 = 'IMAGE_LOAD_FAILED',
						//3 = 'THUMBNAIL_FAILED'
						//alert(error_code);
					}
			
				}).on('change', function(){
					//console.log($(this).data('ace_input_files'));
					//console.log($(this).data('ace_input_method'));
				});
				
			
				//dynamically change allowed formats by changing before_change callback function
				$('#id-file-format').removeAttr('checked').on('change', function() {
					var before_change
					var btn_choose
					var no_icon
					if(this.checked) {
						btn_choose = "Drop images here or click to choose";
						no_icon = "icon-picture";
						before_change = function(files, dropped) {
							var allowed_files = [];
							for(var i = 0 ; i < files.length; i++) {
								var file = files[i];
								if(typeof file === "string") {
									//IE8 and browsers that don't support File Object
									if(! (/\.(jpe?g|png|gif|bmp)$/i).test(file) ) return false;
								}
								else {
									var type = $.trim(file.type);
									if( ( type.length > 0 && ! (/^image\/(jpe?g|png|gif|bmp)$/i).test(type) )
											|| ( type.length == 0 && ! (/\.(jpe?g|png|gif|bmp)$/i).test(file.name) )//for android's default browser which gives an empty string for file.type
										) continue;//not an image so don't keep this file
								}
								
								allowed_files.push(file);
							}
							if(allowed_files.length == 0) return false;
			
							return allowed_files;
						}
					}
					else {
						btn_choose = "Drop files here or click to choose";
						no_icon = "icon-cloud-upload";
						before_change = function(files, dropped) {
							return files;
						}
					}
					var file_input = $('#id-input-file-3');
					file_input.ace_file_input('update_settings', {'before_change':before_change, 'btn_choose': btn_choose, 'no_icon':no_icon})
					file_input.ace_file_input('reset_input');
				});
			
			
			
				
				$('.date-picker').datepicker({autoclose:true}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				$('input[name=date-range-picker]').daterangepicker().prev().on(ace.click_event, function(){
					$(this).next().focus();
				});
				
				$('#timepicker1').timepicker({
					minuteStep: 1,
					showSeconds: true,
					showMeridian: false
				}).next().on(ace.click_event, function(){
					$(this).prev().focus();
				});
				
				$('#colorpicker1').colorpicker();
				$('#simple-colorpicker-1').ace_colorpicker();
			
				
				$(".knob").knob();
				
				
				//we could just set the data-provide="tag" of the element inside HTML, but IE8 fails!
				var tag_input = $('#form-field-tags');
				if(! ( /msie\s*(8|7|6)/.test(navigator.userAgent.toLowerCase())) ) 
				{
					tag_input.tag(
					  {
						placeholder:tag_input.attr('placeholder'),
						//enable typeahead by specifying the source array
						source: ace.variable_US_STATES,//defined in ace.js >> ace.enable_search_ahead
					  }
					);
				}
				else {
					//display a textarea for old IE, because it doesn't support this plugin or another one I tried!
					tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
					//$('#form-field-tags').autosize({append: "\n"});
				}
				
				
				
			
				/////////
				$('#modal-form input[type=file]').ace_file_input({
					style:'well',
					btn_choose:'Drop files here or click to choose',
					btn_change:null,
					no_icon:'icon-cloud-upload',
					droppable:true,
					thumbnail:'large'
				})
				
				//chosen plugin inside a modal will have a zero width because the select element is originally hidden
				//and its width cannot be determined.
				//so we set the width after modal is show
				$('#modal-form').on('shown.bs.modal', function () {
					$(this).find('.chosen-container').each(function(){
						$(this).find('a:first-child').css('width' , '210px');
						$(this).find('.chosen-drop').css('width' , '210px');
						$(this).find('.chosen-search input').css('width' , '200px');
					});
				})
				/**
				//or you can activate the chosen plugin after modal is shown
				//this way select element becomes visible with dimensions and chosen works as expected
				$('#modal-form').on('shown', function () {
					$(this).find('.modal-chosen').chosen();
				})
				*/
			
			});

</script>

<script src="assets/js/ace-elements.min.js"></script>
<script src="assets/js/ace.min.js"></script>

		
	</body>


</html>
