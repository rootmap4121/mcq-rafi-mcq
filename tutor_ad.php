<?php 
include('class/auth_index.php');
$table='ams_add';
$w=80; $h=100; $thumb="photo/";
if(isset($_POST['save']))
{
	
        if(!empty($_POST['name']) && !empty($_POST['address']))
        {
        $exist=array("name"=>$_POST['name']);
        if(!empty($_FILES['image']['name']))
        {
        $files =$obj->image_bigcaption($w,$h,$thumb);
        $photo=substr($files,6,1600);
        }
        else 
        {
            $photo="";
        }
	
    
    $insert=array("name"=>$_POST['name'],"company_name"=>$_POST['company_name'],"address"=>$_POST['address'],"photo"=>$photo,"date" => date('y-m-d'), "status" => 1);
    
    if($obj->exists($table,$exist)==1)
    {
        $error_data[]="<div class='error_msg'>Already Exists</div>";
        $error_flag=true;
        
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$error_data;
            session_write_close();
            header('location:tutor_ad.php');
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $error_data[]="<div class='sucess'>Successfully Saved</div>";
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$error_data;
             session_write_close();
             header('location:tutor_ad.php');
             exit();
         }
     }
 else
     {
      $error_data[]="<div class='error_msg'>Failed to Save</div>";
      $error_flag=true;
      if($error_flag)
      {
        $_SESSION['ERRMSG_ARR']=$error_data;
        session_write_close();
        header('location:tutor_ad.php');
        exit();
      }
     }
    }
}
else 
    {
                        $error_data[]= "<div class='error_msg'>Failed,Some Fields is Empty</div>";
                        $error_flag = true;
                        if ($error_flag) {
                            $_SESSION['ERRMSG_ARR'] = $error_data;
                            session_write_close();
                            header('location:tutor_ad.php');
                            exit();
                        }
     }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>MCQ</title>
        <link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
        <link href="css/style.css" rel="stylesheet" type="text/css" />
        <link href="css/menu.css" rel="stylesheet" type="text/css" />
        <link href="css/style_1.css" rel="stylesheet" type="text/css" />
        <link href="css/background.css" rel="stylesheet" type="text/css" />

        <link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
        <script type="text/javascript">var _siteRoot = 'index.php', _root = 'index.php';</script>
        <script type="text/javascript" src="js/jquery.js"></script>
        <script type="text/javascript" src="js/scripts.js"></script>
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
       
    </head>

    <body>
        <div id="body">
            <?php include("include/head_menu.php"); ?>
            <!----body_main start here-------------------------------------------->
            <div id="body_main">
                <!----head_sec start here--------------------------------------------> 
                <div class="head_sec">

                    <div class="head_logo left">
                        <img src="img/logo.png" />
                    </div>
                    <div class="head_manu left">
                        <?php include("include/menu.php"); ?>
                    </div>

                </div>
                <!----head_sec End here-------------------------------------------->

                <!----welcome start here-------------------------------------------->
                <div id="welcom">
                    <div class="welcom_admin">

                        <div class="wlc shawdow_1 background_14">Welcome to Profile 
                            <div class="wlc_logo right">
                                <ul>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/contact(2).png" /></a></li>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/chat.png" /></a></li>
                                    <li class="right"><a href="#" title="clik to viwe message"><img src="img/message.png" /></a></li>
                                </ul>
                            </div> 
                        </div>
                        <div class="body_adin height_auto">
                            <?php include('include/profile.php'); ?>

                            <div class="mcq left">
                                <div class="mcq_head background_14 shawdow_1 colore_11">
                                    Tutor Add
                                </div>
                                <form action="" method="post" name="add" enctype="multipart/form-data">
                                <div class="mcq_text_fild height_auto shawdow_2">
                                   <?php
                                    if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
                                        foreach($_SESSION['ERRMSG_ARR'] as $msg) 
                                            {
                                    ?>
                                    <span class="label label-warning"><i class="icon-warning-sign bigger-120"></i> <?php echo $msg;  ?> </span>
                                    <?php
                                            }
                                        unset($_SESSION['ERRMSG_ARR']);
                                    }
                                    ?>
                                    <div class="text_div">
                                        <div class="text_name left colore_12">User Name :</div>
                                        <div class="text_fild left">
                                                <input class="textbox width_200 border_none height_30" name="name" value="" placeholder="type user name" type="text" />
                                        </div>
                                    </div>
                                    
                                    <div class="text_div">
                                        <div class="text_name left colore_12">Company Name :</div>
                                        <div class="text_fild left">
                                                <input class="textbox width_200 border_none height_30" name="company_name" value="" placeholder="type company name" type="text" />
                                        </div>
                                    </div>
                                    
                                    <div class="text_div">
                                        <div class="text_name left colore_12">Address :</div>
                                        <div class="text_fild left">
                                                <textarea class="input_msg width_180 height_60" name="address" placeholder="type your address" ></textarea>
                                        </div>
                                    </div>
                                    
                                    <div class="text_div" style="padding-top:10px;">
                                        <div class="text_name left colore_12">Image :</div>
                                        <div class="text_fild left">
                                            <input name="image"  type="file" />
                                        </div>
                                    </div>
                                    
                                    <div class="text_div">
                                        <div class="text_name left colore_12"></div>
                                        <div class="text_fild left">
                                            <button type="reset" name="reset" class="width_90 height_30 background_14 shawdow_1 colore_11" >Reset</button>
                                            
                                            <button type="submit" name="save" class="width_90 height_30 background_14 shawdow_1 colore_11" >submit</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                </form>
                                <div class="clear"></div>
                            </div>
                            <div class="clear"></div>
                        </div>  
                    </div>
                    <!----welcome End here-------------------------------------------->



                </div>
                <!----body_main End here-------------------------------------------->

                <!----fotter start here-------------------------------------------->
                <?php include("include/fotter.php"); ?>
                <!----fotter End here-------------------------------------------->

            </div>

    </body>
</html>
