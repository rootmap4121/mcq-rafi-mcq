<?php 
session_start();
$error_data[]='';
$error_flag=false;
include('class/db_Class_index.php');
$obj=new db_class();
$table='ams_student';
if(isset($_POST['submit']))
{
   $exits=array("user_name"=>$_POST['name']);
   $insert=array ("user_name"=>$_POST['name'],"s_mobile"=>$_POST['number'],"pass"=>$_POST['password'],"date"=>date('Y-m-d'),"status"=>1);
   
   if($obj->exists($table,$exits)==1)
           {
        $error_data[]="<div class='error_msg'>Already Exists</div>";
        $error_flag=true;
        
        if($error_flag)
        {
            $_SESSION['ERRMSG_ARR']=$error_data;
            session_write_close();
            header('location: singup.php');
            exit();
        }
    }
 else
    {
     if($obj->insert($table,$insert)==1)
     {
         $error_data[]="<div class='sucess'>Successfully Saved</div>";
         $error_flag=true;
         if($error_flag)
         {
             $_SESSION['SMSG_ARR']=$error_data;
             session_write_close();
             header('location: welcome.php');
             exit();
         }
     }
 else
     {
      $error_data[]='Failed to Save';
      $error_flag=true;
      if($error_flag)
      {
        $_SESSION['ERRMSG_ARR']=$error_data;
        session_write_close();
        header('location: index.php');
        exit();
      }
     }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link href="css/background.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<body>
    <div id="body">
    <?php include("include/add.php");?>    
   <?php include("include/index_head_menu.php");?>
    
<!----body_main start here-------------------------------------------->
<div id="body_main" class="shawdow_1 background_18">
       
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       <div class="head_logo left">
         <img src="img/logo.png" />
       </div>
       <div class="head_manu left">
         <?php include("include/menu.php");?>
       </div> 
      </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----gallery start here-------------------------------------------->
     <?php include("include/gallery.php");?>
     <!----gallery End here-------------------------------------------->
     
     <!----madl_part_1 start here-------------------------------------------->
     <?php include("include/mdl_menu.php");?>
     <!----madl_part_1 End here-------------------------------------------->
     
     <!----madl_part_2 start here-------------------------------------------->
     <div id="madl_part_2">
         <div class="add height_120 background_14 shawdow_1 left">
             <a href="#">  <img src="img/add_banner.jpg"/></a>
         </div>
         
         <div class="add height_120 background_14 shawdow_1 left margin_left_5">
             <a href="#">  <img src="img/add_banner.jpg"/></a>
         </div>
         
         <div class="add height_120 background_14 shawdow_1 right">
             <a href="#">  <img src="img/add_banner.jpg"/></a>
         </div>
     </div>
     <!----madl_part_2 End here-------------------------------------------->
     
     <div class="clear"></div>
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
    <?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
