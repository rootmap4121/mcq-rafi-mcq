<?php 
include('class/auth_index.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link href="css/background.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script>
            function showUser(str)
            {
                if (str == "")
                {
                    document.getElementById("subject").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("subject").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/test_paper.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
        <script>
            function showboard(str)
            {
                if (str == "")
                {
                    document.getElementById("board").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("board").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/test_paper_category.php?q="+str, true);
                xmlhttp.send();
            }
        </script>

</head>
	
<body>
<div id="body">
  <?php include("include/head_menu.php");?>
<!----body_main start here-------------------------------------------->
   <div id="body_main">
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       
       <div class="head_logo left">
         <img src="img/logo.png" />
       </div>
       <div class="head_manu left">
         <?php include("include/menu.php");?>
       </div>
       
       </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----welcome start here-------------------------------------------->
     <div id="welcom">
       <div class="welcom_admin">
         
           <div class="wlc shawdow_1 background_14">Welcome to Profile 
             <div class="wlc_logo right">
               <ul>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/contact(2).png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/chat.png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/message.png" /></a></li>
               </ul>
             </div> 
            </div>
            
            <div class="body_adin height_400">
            
            <div class="proflie left ">
               <div class="chaneg background_14 shawdow_1">
                 <ul class="margin_left_5">
                 <h1>Change and Viwe Status</h1>
                    <li><a href="" title="clik here change your images">Change Images</a></li>
                    <li><a href="" title="clik here edit your profile">Edit Profile</a></li>
                    <li><a href="" title="clik here change your password">Change Password</a></li>
                    <li><a href="">Logine Time : 12:30pm</a></li>
                    <li><a href="">User Name : Rasel</a></li>
                    <li><a href="">Browser : Mozila</a></li>
                    <li><a href="">Last Loign Time : 10:20am</a></li>
                    <li><a href="">IP NO : 119.19.1.1</a></li>
                 </ul>
               </div>
            </div>
            
           <div class="mcq left">
             <div class="mcq_head background_14 shawdow_1 colore_11">
               PSC Online MCQ Model Test
             </div>
          <!-------massege Start here------------->
                <div class="msg">
                </div>
           <!-------massege End here------------->
             <div class="mcq_text_fild margin_top_30">
               <div class="text_div">

                 <div class="text_name left colore_12">Medium :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select name="medium" onchange="showUser(this.value)">
                      <option value="">medium name</option>
                       <?php 
						  $medium=$obj->selectAll('ams_madiam');
						  foreach($medium as $romd):
						?>
                        <option value="<?php echo $romd->id;?>"><?php echo $romd->medium_name;?></option>
                        <?php endforeach;?>
                      </select>
                    </div>
                 </div>
               </div>
               
               
               <div class="text_div">
                 <div class="text_name left colore_12">Subject :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select name="subject" id="subject" >
                       <option>Subject</option>
                      </select>
                    </div>
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_name left colore_12">Category :</div>
                 <div class="text_fild left">
                   
                    <label class="font"> <input onchange="showboard(this.value)" type="radio" value="1" name="select"/> Board</label>  
                    <label class="font"><input onchange="showboard(this.value)" type="radio" value="2" name="select"/> School</label>
                    
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_name left colore_12">School/Board :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select name="board" id="board">
                        <option value="">Select</option>
                      </select>
                    </div>
                 </div>
               </div>
               
               <div class="text_div">
                 <div class="text_name left colore_12">Year :</div>
                 <div class="text_fild left">
                   <div class="select-style">
                      <select>
                        <option value="">Year</option>
                        <option value="">Half year</option>
                        <option value="">anual</option>
                      </select>
                    </div>
                 </div>
               </div>
                 
             </div>
           </div>
            </div>
           <table class="CSSTableGenerator width_100p" >
                    <tr>
                        <td>
                            SL NO
                        </td>
                        <td >
                           Name
                        </td>
                        <td>
                            View
                        </td>
                        <td>
                            Download
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                   <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            1
                        </td>
                        <td>
                            Write another letter 
                        </td>
                        <td>
                           <a title="clik here to view video" href="#"><img src="img/video_television.gif"/></a>
                        </td>
                        <td>
                           <a title="clik here to download file" href="#"><img src="img/download.gif"/></a>
                        </td>
                    </tr> 
                   
                    
                </table>
           <div class="clear"></div>
       </div>  
     </div>
     <!----welcome End here-------------------------------------------->
     
    
     
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
<?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
