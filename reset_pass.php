<?php
session_start();
$error_data[]='';
$error_flag=false;
include('class/db_Class_index.php');
$obj=new db_class();
if(isset($_POST['login']))
{
	if($obj->exists_multiple("ams_student",array("user_name"=>$_POST['username'],"pass"=>$_POST['password'])))
	{
		session_regenerate_id();
		$_SESSION['SESS_AMSIT_USERLOGIN_KEY']=$obj->SelectAllByVal("ams_student","user_name",$_POST['username'],"id");
		$_SESSION['SESS_USERNAME']=$obj->SelectAllByVal("ams_student","user_name",$_POST['username'],"user_name");
		$_SESSION['SESS_STATUS']=$obj->SelectAllByVal("ams_student","user_name",$_POST['username'],"status");
		session_write_close();
		header('location: welcome.php');
		//echo "login ";
		exit();	
	}
	else
	{
		$error_data[]="<div class='error_msg'>Login Failed, Please Try Again</div>";
		$error_flag=true;
		if($error_flag)
		{
			$_SESSION['ERRMSG_ARR']=$error_data;
			session_write_close();
			header('location:'.$obj->filename());
			exit();
			
		}	
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/background.css" rel="stylesheet" type="text/css" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<body>
<div id="body">
<?php include("include/index_head_menu.php");?>
<!----body_main start here-------------------------------------------->
   <div id="body_main">
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       <div class="head_logo left">
         <a href="#"><img src="img/logo.png" alt="" /></a>
       </div>
       <div class="head_manu left">
         
         <?php include("include/menu.php");?>
       </div>
       </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----login start here-------------------------------------------->
     <div id="login" class="height_290 background_18">
         <div class="head_loging background_14 shawdow_1">
             <div class="instrc left background_19">
                Reset Your Password
               </div>
         </div>
         <div class="gallery_loging left">
           
         
<ul class="background_14 shawdow_1">
 <?php
if( isset($_SESSION['ERRMSG_ARR']) && is_array($_SESSION['ERRMSG_ARR']) && count($_SESSION['ERRMSG_ARR']) >0 ) {
    foreach($_SESSION['ERRMSG_ARR'] as $msg) 
        {
?>
<span class="label label-warning"><i class="icon-warning-sign bigger-120"></i> <?php echo $msg;  ?> </span>
<?php
        }
    unset($_SESSION['ERRMSG_ARR']);
}
?>
           <form action="" method="post" name="login_form">
            <li class="left" style="width:130px;margin-left:55px; margin-top:40px;">E-mail :</li>
            
             <li class="left"  style="margin-top:40px;"><input name="username" class="textbox" placeholder="type yoer @ email" type="email" /></li>
             
              
               <li class="left" style="width:300px;margin-left:55px;text-align:center;">
                  <a href="reset_user_name.php">forget my user name</a>
               </li> 
               
                
               <li class="left" style="width:300px;margin-left:55px;text-align:center;">
               <button  type="submit" class="background_14 colore_11 shawdow_1" name="login">Submit</button></li>
           </form>
          </ul>
         
         </div>
         
           <div class="logo_login right background_14 shawdow_1">
             <img src="img/Login.png" />
           </div>
         
       </div>
    
     <!----login End here-------------------------------------------->
     
     
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
<?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
