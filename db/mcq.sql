-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 10, 2014 at 08:10 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mcq`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE IF NOT EXISTS `admin` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `gender` varchar(200) NOT NULL,
  `number` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `password`, `username`, `email`, `gender`, `number`, `date`, `status`) VALUES
(1, 'admin', '123456', 'admin', 'rasel_alone@yahoo.com', 'female', '01675180510', '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_access_log`
--

CREATE TABLE IF NOT EXISTS `ams_access_log` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `user_ip` int(20) DEFAULT NULL,
  `discptn` text,
  `access_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_access_log`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_add`
--

CREATE TABLE IF NOT EXISTS `ams_add` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `img` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ams_add`
--

INSERT INTO `ams_add` (`id`, `name`, `address`, `img`, `date`, `status`) VALUES
(9, 'Add Here', 'Md. Rafi Ahammed\r\nExecutive Director\r\nRAFIMCQ, bangladesh\r\nHouse-6, Road-11, Kalyanpur, Dhaka-1207,\r\nBangladesh.\r\nE-mail: rafimcq@gmail.com\r\nMobile: +88-01715-909595', '', '2014-04-10', 1),
(4, 'gmggj', 'gjghjgh', '', '2014-04-10', 1),
(6, 'dfdsf', 'dsfsfgfs', '', '2014-04-10', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_answer`
--

CREATE TABLE IF NOT EXISTS `ams_answer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ans_name` varchar(200) DEFAULT NULL,
  `subject_id` int(20) DEFAULT NULL,
  `class_id` int(20) DEFAULT NULL,
  `ans_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `ams_answer`
--

INSERT INTO `ams_answer` (`id`, `ans_name`, `subject_id`, `class_id`, `ans_date`, `status`) VALUES
(10, 'dfsf', 4, 11, '2014-03-18', 1),
(11, 'ds', 4, 11, '2014-03-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_block_visitor`
--

CREATE TABLE IF NOT EXISTS `ams_block_visitor` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `visitor_ip` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_block_visitor`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_board_school`
--

CREATE TABLE IF NOT EXISTS `ams_board_school` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `ams_board_school`
--

INSERT INTO `ams_board_school` (`id`, `name`, `date`, `status`) VALUES
(8, 'dhaka', '2014-04-01', 1),
(9, 'amsit', '2014-04-01', 2),
(10, 'rtrrt', '2014-04-01', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ams_category`
--

CREATE TABLE IF NOT EXISTS `ams_category` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `medium_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `ams_category`
--

INSERT INTO `ams_category` (`id`, `name`, `medium_id`, `date`, `status`) VALUES
(13, 'MCQ', 8, '2014-04-01', 1),
(14, 'MCQ', 9, '2014-04-01', 1),
(15, 'MCQ Quiz', 8, '2014-04-01', 1),
(16, 'MCQ Quiz', 9, '2014-04-01', 1),
(17, 'MCQ Test Paper', 8, '2014-04-01', 1),
(18, 'MCQ Test Paper', 9, '2014-04-01', 1),
(19, 'MCQ job', 8, '2014-04-01', 1),
(20, 'MCQ job', 9, '2014-04-01', 1),
(21, 'Composition', 8, '2014-04-01', 1),
(22, 'Composition', 9, '2014-04-01', 1),
(23, 'Video Class', 8, '2014-04-01', 1),
(24, 'Video Class', 9, '2014-04-01', 1),
(27, 'Suggestion', 8, '2014-04-01', 1),
(28, 'Suggestion', 9, '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_chating`
--

CREATE TABLE IF NOT EXISTS `ams_chating` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `uesre_id` int(20) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_chating`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_chat_msg`
--

CREATE TABLE IF NOT EXISTS `ams_chat_msg` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `msg` text,
  `for_user_id` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_chat_msg`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_class`
--

CREATE TABLE IF NOT EXISTS `ams_class` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `class_name` varchar(200) DEFAULT NULL,
  `medium_id` int(20) NOT NULL,
  `class_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=126 ;

--
-- Dumping data for table `ams_class`
--

INSERT INTO `ams_class` (`id`, `class_name`, `medium_id`, `class_date`, `status`) VALUES
(94, 'PSC', 8, '2014-04-01', 13),
(95, 'PSC', 9, '2014-04-01', 14),
(96, 'Class-XI', 8, '2014-04-01', 13),
(97, 'Class-XI', 9, '2014-04-01', 14),
(98, 'Class-VI', 8, '2014-04-01', 13),
(99, 'Class-VI', 9, '2014-04-01', 14),
(100, 'JSC', 8, '2014-04-01', 13),
(101, 'JSC', 8, '2014-04-01', 15),
(102, 'PSC', 8, '2014-04-01', 15),
(103, 'Class-XI', 8, '2014-04-01', 15),
(104, 'Class-VI', 8, '2014-04-01', 15),
(105, 'PSC', 9, '2014-04-01', 16),
(106, 'Class-XI', 9, '2014-04-01', 16),
(107, 'Class-VI', 9, '2014-04-01', 16),
(108, 'JSC', 9, '2014-04-01', 16),
(109, 'JSC', 9, '2014-04-01', 14),
(110, 'SSC', 8, '2014-04-01', 13),
(111, 'SSC', 9, '2014-04-01', 14),
(112, 'HSC', 8, '2014-04-01', 13),
(113, 'HSC', 9, '2014-04-01', 14),
(114, 'PSC', 8, '2014-04-01', 17),
(115, 'PSC', 9, '2014-04-01', 18),
(116, 'JSC', 8, '2014-04-01', 17),
(117, 'JSC', 9, '2014-04-01', 18),
(118, 'Class-XI', 8, '2014-04-01', 17),
(119, 'Class-XI', 9, '2014-04-01', 18),
(120, 'Class-VI', 8, '2014-04-01', 17),
(121, 'Class-VI', 9, '2014-04-01', 18),
(122, 'HSC', 8, '2014-04-01', 17),
(123, 'HSC', 9, '2014-04-01', 18),
(124, 'SSC', 8, '2014-04-01', 17),
(125, 'SSC', 9, '2014-04-01', 18);

-- --------------------------------------------------------

--
-- Table structure for table `ams_composition`
--

CREATE TABLE IF NOT EXISTS `ams_composition` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_composition`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_exam`
--

CREATE TABLE IF NOT EXISTS `ams_exam` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `exam_name` varchar(200) DEFAULT NULL,
  `class_id` int(20) DEFAULT NULL,
  `exam_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `ams_exam`
--

INSERT INTO `ams_exam` (`id`, `exam_name`, `class_id`, `exam_date`, `status`) VALUES
(18, 'Haif Yearly', 81, '2014-03-23', 1),
(19, 'Annual', 81, '2014-03-23', 1),
(20, 'Edexcel', 84, '2014-03-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_img`
--

CREATE TABLE IF NOT EXISTS `ams_img` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ams_img`
--

INSERT INTO `ams_img` (`id`, `name`, `date`, `status`) VALUES
(1, 'school', '2014-04-09', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_instet`
--

CREATE TABLE IF NOT EXISTS `ams_instet` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `inst_name` varchar(200) DEFAULT NULL,
  `ins_date` date NOT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ams_instet`
--

INSERT INTO `ams_instet` (`id`, `inst_name`, `ins_date`, `status`) VALUES
(3, 'amsitsofts', '2014-03-18', 1),
(4, 'cccc', '2014-03-15', 1),
(5, 'xZx', '2014-03-15', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_job`
--

CREATE TABLE IF NOT EXISTS `ams_job` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ams_job_name` varchar(200) NOT NULL,
  `user_id` int(20) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_job`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_madiam`
--

CREATE TABLE IF NOT EXISTS `ams_madiam` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_name` varchar(200) DEFAULT NULL,
  `class_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ams_madiam`
--

INSERT INTO `ams_madiam` (`id`, `medium_name`, `class_id`, `category_id`, `date`, `status`) VALUES
(8, 'English', 77, 10, '2014-03-23', 1),
(9, 'Bangla', 45, 5, '2014-03-23', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq`
--

CREATE TABLE IF NOT EXISTS `ams_mcq` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `medium_id` int(20) NOT NULL,
  `subject_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_composition_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_composition_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_composition_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `subject_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` text NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_job_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_job_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_job_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_quiz`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_quiz` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_quiz`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_quiz_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_quiz_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_quiz_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_suggetion_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_suggetion_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_suggetion_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_testpaper_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_testpaper_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `detail` text NOT NULL,
  `category_id` int(20) NOT NULL,
  `board` varchar(200) NOT NULL,
  `year_date` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ams_mcq_testpaper_data`
--

INSERT INTO `ams_mcq_testpaper_data` (`id`, `name`, `medium_id`, `class_id`, `detail`, `category_id`, `board`, `year_date`, `date`, `status`) VALUES
(5, 'dfgdgd', 8, 114, 'dgdgd', 1, '2', 1, '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_mcq_video_data`
--

CREATE TABLE IF NOT EXISTS `ams_mcq_video_data` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `medium_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `qustion_id` int(20) NOT NULL,
  `answer_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_mcq_video_data`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_multipel_answer`
--

CREATE TABLE IF NOT EXISTS `ams_multipel_answer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `question_id` int(20) NOT NULL,
  `category_id` int(20) NOT NULL,
  `class_id` int(20) NOT NULL,
  `medium_id` int(20) NOT NULL,
  `subject_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=104 ;

--
-- Dumping data for table `ams_multipel_answer`
--

INSERT INTO `ams_multipel_answer` (`id`, `name`, `question_id`, `category_id`, `class_id`, `medium_id`, `subject_id`, `date`, `status`) VALUES
(90, '11', 55, 16, 106, 9, 26, '2014-04-01', 16),
(91, 'vala', 56, 14, 95, 9, 29, '2014-04-01', 14),
(92, 'khob vala', 56, 14, 95, 9, 29, '2014-04-01', 14),
(93, 'beshi vala', 56, 14, 95, 9, 29, '2014-04-01', 14),
(94, 'vala na', 56, 14, 95, 9, 29, '2014-04-01', 14),
(95, 'moja', 56, 14, 95, 9, 29, '2014-04-01', 14),
(96, 'jsc b1', 58, 14, 109, 9, 30, '2014-04-01', 14),
(97, 'jsc b2', 58, 14, 109, 9, 30, '2014-04-01', 14),
(98, 'jsc b3', 58, 14, 109, 9, 30, '2014-04-01', 14),
(99, 'jsc b4', 58, 14, 109, 9, 30, '2014-04-01', 14),
(100, 'ssc 1', 61, 14, 111, 9, 32, '2014-04-01', 14),
(101, 'ssc 2', 60, 13, 110, 8, 33, '2014-04-01', 13),
(102, 'hsc e 1', 62, 13, 112, 8, 35, '2014-04-01', 13),
(103, 'hsc b1', 63, 14, 113, 9, 34, '2014-04-01', 14);

-- --------------------------------------------------------

--
-- Table structure for table `ams_multipel_select_answer`
--

CREATE TABLE IF NOT EXISTS `ams_multipel_select_answer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `answer_id` int(20) NOT NULL,
  `question_id` int(20) NOT NULL,
  `medium_id` int(20) NOT NULL,
  `subject_id` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `ams_multipel_select_answer`
--

INSERT INTO `ams_multipel_select_answer` (`id`, `answer_id`, `question_id`, `medium_id`, `subject_id`, `date`, `status`) VALUES
(17, 93, 56, 0, 0, '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_notes`
--

CREATE TABLE IF NOT EXISTS `ams_notes` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `notes_name` varchar(200) DEFAULT NULL,
  `notes_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `ams_notes`
--

INSERT INTO `ams_notes` (`id`, `notes_name`, `notes_date`, `status`) VALUES
(3, 'eeeeeeeee', '2014-03-18', 1),
(4, 'scfafafffffffff', '2014-03-18', 1),
(5, 'fgg', '2014-03-16', 1),
(6, 'cccccc', '2014-03-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_notic`
--

CREATE TABLE IF NOT EXISTS `ams_notic` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `notic_name` varchar(200) DEFAULT NULL,
  `details` text NOT NULL,
  `notic_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `ams_notic`
--

INSERT INTO `ams_notic` (`id`, `notic_name`, `details`, `notic_date`, `status`) VALUES
(9, 'Holly Day', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.                                 \r\n                                                    ', '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_question`
--

CREATE TABLE IF NOT EXISTS `ams_question` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `ques_name` varchar(200) DEFAULT NULL,
  `medium_id` int(20) NOT NULL,
  `subject_id` int(20) DEFAULT NULL,
  `category` int(20) NOT NULL,
  `class` int(20) NOT NULL,
  `ques_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `ams_question`
--

INSERT INTO `ams_question` (`id`, `ques_name`, `medium_id`, `subject_id`, `category`, `class`, `ques_date`, `status`) VALUES
(36, 'hffgh fghfgh ghgh', 9, 8, 0, 0, '2014-03-30', 1),
(37, 'fhfhhfghf', 8, 12, 0, 0, '2014-03-30', 1),
(38, 'hgfghfghf', 8, 12, 0, 0, '2014-03-30', 1),
(39, 'fhfghfgh', 8, 9, 0, 0, '2014-03-30', 1),
(40, 'hjghjj', 8, 9, 0, 0, '2014-03-30', 1),
(41, 'jgjklkl', 8, 9, 0, 0, '2014-03-30', 1),
(46, 'how to creat a table', 8, 9, 10, 92, '2014-04-01', 1),
(47, 'how to creat a fffff', 9, 8, 10, 92, '2014-04-01', 1),
(48, 'how to creat a table', 9, 18, 14, 95, '2014-04-01', 1),
(50, 'how to creat a table', 9, 25, 16, 106, '2014-04-01', 1),
(51, 'CBDDGDG', 9, 26, 16, 106, '2014-04-01', 1),
(52, 'how to creat a tingtong', 9, 26, 16, 106, '2014-04-01', 1),
(53, 'how to creat a tableuuuu', 8, 28, 13, 94, '2014-04-01', 1),
(54, 'how to creat a tableeeeeeee', 8, 28, 13, 94, '2014-04-01', 1),
(55, 'fahad vai', 9, 26, 16, 106, '2014-04-01', 1),
(56, 'ki khobor?????', 9, 29, 14, 95, '2014-04-01', 1),
(57, 'Test Psc', 8, 28, 13, 94, '2014-04-01', 1),
(58, 'Test jsc b', 9, 30, 14, 109, '2014-04-01', 1),
(59, 'Test jsc e', 8, 31, 13, 100, '2014-04-01', 1),
(60, 'Test ssc b', 8, 33, 13, 110, '2014-04-01', 1),
(61, 'Test ssc e', 9, 32, 14, 111, '2014-04-01', 1),
(62, 'test hsc e', 8, 35, 13, 112, '2014-04-01', 1),
(63, 'test hsc b', 9, 34, 14, 113, '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_question_answer`
--

CREATE TABLE IF NOT EXISTS `ams_question_answer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `subject` int(20) NOT NULL,
  `question` int(20) NOT NULL,
  `answer` int(20) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ams_question_answer`
--

INSERT INTO `ams_question_answer` (`id`, `subject`, `question`, `answer`, `date`, `status`) VALUES
(1, 6, 5, 6, '2014-03-18', 1),
(2, 5, 15, 6, '2014-03-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_student`
--

CREATE TABLE IF NOT EXISTS `ams_student` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fname` varchar(255) DEFAULT NULL,
  `lname` varchar(200) NOT NULL,
  `m_name` varchar(100) DEFAULT NULL,
  `f_name` varchar(100) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `gender` varchar(100) DEFAULT NULL,
  `region` varchar(100) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `s_mobile` varchar(255) DEFAULT NULL,
  `f_mubile` varchar(255) DEFAULT NULL,
  `inst_id` varchar(200) NOT NULL,
  `national` varchar(200) NOT NULL,
  `addrs` varchar(200) NOT NULL,
  `img` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `pass` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `ams_student`
--

INSERT INTO `ams_student` (`id`, `fname`, `lname`, `m_name`, `f_name`, `email`, `gender`, `region`, `dob`, `s_mobile`, `f_mubile`, `inst_id`, `national`, `addrs`, `img`, `user_name`, `pass`, `date`, `status`) VALUES
(16, 'sheikh', 'rasel', 'bvskvk', 'alimuddin', 'rasel_alone@yahoo.com', 'male', 'bangladesh', '1988-10-11', 'dfdf', 'dfdf', 'amsitsofts', 'Nationality', 'babu', '', 'babu', '1111111', '2014-03-19', 1),
(17, 'fss', 'sdfsd', 'sffs', 'sfsd', 'fdfdddf', 'male', 'bangladesh', '2014-02-24', 'dffd', 'dfgdf', 'amsitsofts', 'Nationality', 'fdf', '', 'fdfd', '45465656', '2014-03-22', 1),
(18, 'fdd', 'gfhfghfgh', 'chf', 'fgdfgdg', 'cgfhfgf', 'male', 'bangladesh', '2014-03-03', 'bdd', 'dffd', 'amsitsofts', 'Nationality', 'fdfdf', '', 'rasel', 'dfgdfd', '2014-03-22', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_student_answer`
--

CREATE TABLE IF NOT EXISTS `ams_student_answer` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `answer` text NOT NULL,
  `number` int(20) NOT NULL,
  `date` date NOT NULL,
  `stutas` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_student_answer`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_subject`
--

CREATE TABLE IF NOT EXISTS `ams_subject` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sub_name` varchar(200) DEFAULT NULL,
  `madiam_id` int(20) DEFAULT NULL,
  `category` int(20) NOT NULL,
  `class_id` int(20) DEFAULT NULL,
  `sub_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `ams_subject`
--

INSERT INTO `ams_subject` (`id`, `sub_name`, `madiam_id`, `category`, `class_id`, `sub_date`, `status`) VALUES
(13, 'math', 8, 0, 94, '2014-04-01', 1),
(14, 'bangla', 8, 0, 94, '2014-04-01', 1),
(15, 'bangla', 8, 0, 98, '2014-04-01', 1),
(16, 'math', 8, 0, 96, '2014-04-01', 1),
(17, 'math', 8, 0, 98, '2014-04-01', 1),
(18, 'math', 9, 0, 95, '2014-04-01', 1),
(19, 'math', 9, 0, 97, '2014-04-01', 1),
(20, 'math', 9, 0, 99, '2014-04-01', 1),
(21, 'ggggggg', 8, 0, 94, '2014-04-01', 1),
(22, 'dddddddd', 8, 0, 96, '2014-04-01', 1),
(23, 'gdfgdgdf', 8, 0, 98, '2014-04-01', 1),
(24, 'bcvbs', 8, 0, 96, '2014-04-01', 1),
(25, 'ssadS', 9, 0, 106, '2014-04-01', 1),
(26, 'CCBCC', 9, 0, 106, '2014-04-01', 1),
(27, 'nadim', 8, 0, 94, '2014-04-01', 1),
(28, 'islam', 8, 0, 94, '2014-04-01', 1),
(29, 'tiring biring', 9, 0, 95, '2014-04-01', 1),
(30, 'jsc', 9, 0, 109, '2014-04-01', 1),
(31, 'jsc e', 8, 0, 100, '2014-04-01', 1),
(32, 'Test SSC', 9, 0, 111, '2014-04-01', 1),
(33, 'Test SSC', 8, 0, 110, '2014-04-01', 1),
(34, 'hsc b', 9, 0, 113, '2014-04-01', 1),
(35, 'hsc e', 8, 0, 112, '2014-04-01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_summerry`
--

CREATE TABLE IF NOT EXISTS `ams_summerry` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sumry_name` varchar(200) DEFAULT NULL,
  `sumrydate` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `ams_summerry`
--

INSERT INTO `ams_summerry` (`id`, `sumry_name`, `sumrydate`, `status`) VALUES
(3, 'fff', '2014-03-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_susetion`
--

CREATE TABLE IF NOT EXISTS `ams_susetion` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `sus_name` varchar(200) DEFAULT NULL,
  `madiam_id` int(20) DEFAULT NULL,
  `class` int(20) DEFAULT NULL,
  `sub_id` int(20) DEFAULT NULL,
  `sus_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ams_susetion`
--

INSERT INTO `ams_susetion` (`id`, `sus_name`, `madiam_id`, `class`, `sub_id`, `sus_date`, `status`) VALUES
(2, 'asdfa', 4, 11, 1, '2014-03-16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_teacher`
--

CREATE TABLE IF NOT EXISTS `ams_teacher` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `teacher_name` varchar(200) DEFAULT NULL,
  `inst_name` varchar(200) DEFAULT NULL,
  `tchr_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `ams_teacher`
--

INSERT INTO `ams_teacher` (`id`, `teacher_name`, `inst_name`, `tchr_date`, `status`) VALUES
(2, 'ddd', '4', '2014-03-18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ams_test_paper`
--

CREATE TABLE IF NOT EXISTS `ams_test_paper` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_test_paper`
--


-- --------------------------------------------------------

--
-- Table structure for table `ams_visitor`
--

CREATE TABLE IF NOT EXISTS `ams_visitor` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `visitor_ip` varchar(200) DEFAULT NULL,
  `visitor_date` date DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ams_visitor`
--


-- --------------------------------------------------------

--
-- Table structure for table `ans_video_class`
--

CREATE TABLE IF NOT EXISTS `ans_video_class` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `date` date NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ans_video_class`
--


-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `date` text NOT NULL,
  `status` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `username`, `password`, `date`, `status`) VALUES
(1, 'rasel', 'babu', '123456', '', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
