<?php 
include('class/auth_index.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>MCQ</title>
<link rel="shortcut icon" href="img/graduate.bmp" type="image/x-icon" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/menu.css" rel="stylesheet" type="text/css" />
<link href="css/style_1.css" rel="stylesheet" type="text/css" />
<link href="css/background.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="css/slide.css" type="text/css" media="screen" />
<script type="text/javascript">var _siteRoot='index.php',_root='index.php';</script>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/scripts.js"></script>
<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
<script>
            function showmedium(str)
            {
                if (str == "")
                {
                    document.getElementById("medium").innerHTML = "";
                    return;
                }
                if (window.XMLHttpRequest)
                {// code for IE7+, Firefox, Chrome, Opera, Safari
                    xmlhttp = new XMLHttpRequest();
                }
                else
                {// code for IE6, IE5
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        document.getElementById("medium").innerHTML = xmlhttp.responseText;
                    }
                }
                xmlhttp.open("GET", "ajax/test_paper.php?q="+str, true);
                xmlhttp.send();
            }
        </script>
</head>
	
<body>
<div id="body">
  <?php include("include/head_menu.php");?>
<!----body_main start here-------------------------------------------->
   <div id="body_main">
    <!----head_sec start here--------------------------------------------> 
     <div class="head_sec">
       
       <div class="head_logo left">
         <img src="img/logo.png" />
       </div>
       <div class="head_manu left">
         <?php include("include/menu.php");?>
       </div>
       
       </div>
     <!----head_sec End here-------------------------------------------->
     
     <!----welcome start here-------------------------------------------->
     <div id="welcom">
       <div class="welcom_admin">
         
           <div class="wlc shawdow_1 background_14">Welcome to Profile 
             <div class="wlc_logo right">
               <ul>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/contact(2).png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/chat.png" /></a></li>
                 <li class="right"><a href="#" title="clik to viwe message"><img src="img/message.png" /></a></li>
               </ul>
             </div> 
            </div>
            
            <div class="paragraph">
              <div class="paragraph_proflie height_210 margin_left_10 background_14 shawdow_1 left">
                <ul class="margin_left_5">
                 <h1>Change and Viwe Status</h1>
                    <li><a href="" title="clik here change your images">Change Images</a></li>
                    <li><a href="" title="clik here edit your profile">Edit Profile</a></li>
                    <li><a href="" title="clik here change your password">Change Password</a></li>
                    <li><a href="">Logine Time : 12:30pm</a></li>
                    <li><a href="">User Name : Rasel</a></li>
                    <li><a href="">Browser : Mozila</a></li>
                    <li><a href="">Last Loign Time : 10:20am</a></li>
                    <li><a href="">IP NO : 119.19.1.1</a></li>
                 </ul>
              </div>
              
              <div class="paragraph_head left background_14 shawdow_1  margin_top_5 font colore_11">
                 MCQ Quiz Class VI
              </div>
              <div class="paragraph_search left shawdow_1  margin_top_5 font">
             <div class="select left">
                     <select class="" onchange="showmedium(this.value)">
                        <option value="">Select medium</option>
                       <?php 
						  $medium=$obj->selectAll('ams_madiam');
						  foreach($medium as $romd):
						?>
                        <option value="<?php echo $romd->id;?>"><?php echo $romd->medium_name;?></option>
                        <?php endforeach;?>
                      </select>
             </div>
             
             <div class="select left margin_left_5">
                    <select class="" name="medium" id="medium" onchange="showanswer(this.value)">
                        <option value="">Select Subject</option>
                      </select>
             </div>
                 <button class="background_14 margin_left_5 margin_top_10 shawdow_1 colore_11 height_30 width_50">GO</button>
              </div>
                     <form action="" method="post" name="answer"> 
                 <div class="show_question left height_auto background_14 shawdow_1">
                   <ul class="">
                     <h1 class="colore_11">Q1. হিসাববিজ্ঞানের বিশেষ উদ্দেশ্য কোনটি?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="f"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="f"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="f"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="f"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   
                   <ul class="">
                     <h1 class="colore_11">Q1. ব্যবসায়ের মোট সম্পদ ও দায়-দেনা সম্পর্কে জানা যায় কিসের মাধ্যমে?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="r"/>অর্থনৈতিক তরা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="r"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="r"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="r"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   
                   <ul class="">
                     <h1 class="colore_11">Q1. হিসাববিজ্ঞানের বিশেষ উদ্দেশ্য কোনটি?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="c"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="c"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="c"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="c"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   
                   <ul class="">
                     <h1 class="colore_11">Q1. হিসাববিজ্ঞানের বিশেষ উদ্দেশ্য কোনটি?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="t"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="t"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="t"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="t"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   
                   <ul class="">
                     <h1 class="colore_11">Q1. হিসাববিজ্ঞানের বিশেষ উদ্দেশ্য কোনটি?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="j"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="j"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="j"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="j"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   
                   <ul class="">
                     <h1 class="colore_11">Q1. হিসাববিজ্ঞানের বিশেষ উদ্দেশ্য কোনটি?</h1>
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="k"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="k"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>       
                      <li class="left margin_left_40 margin_top_15"><label><input type="radio" value="" name="k"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                      <li class="left margin_left_100 margin_top_15"><label><input type="radio" value="" name="k"/>অর্থনৈতিক তথ্য পরিবেশন করা</label></li>
                   </ul>
                   <button type="submit" class="background_14 shawdow_1 colore_11 margin_left_200 margin_top_10">Submit</button>
                   <button type="reset" class="background_14 shawdow_1 colore_11">Reset</button>
                 </div>
                </form>
          
            
            </div>
           <div class="clear"></div>
       </div>  
     </div>
     <!----welcome End here-------------------------------------------->
     
    
     
   </div>
<!----body_main End here-------------------------------------------->

<!----fotter start here-------------------------------------------->
<?php include("include/fotter.php");?>
<!----fotter End here-------------------------------------------->

</div>

</body>
</html>
